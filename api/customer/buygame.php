<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/custgame.php';
    require_once ROOT . '/models/custgame_manager.php';
    require_once ROOT . '/models/price.php';
    require_once ROOT . '/models/price_manager.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if (isset($_POST["game_id"])) {
            $buy = CustGame::newCustGame($_POST["game_id"],0);
            if ($buy !== NULL) {
                $price = PriceManager::findPriceByGameId($_POST["game_id"])[0];
                $user = CustomerManager::findCustomerById($_SESSION["id"]);
                if($price->getPrice() <= $user->getMoney()) {
                    $user->setMoney($user->getMoney() - $price->getPrice());
                    $user->save();
                    if(CustGameManager::createCustGame($buy) == 0) {
                        echo json_encode(array(
                            'ret_code' => 0,
                            'data' => $buy->toJSON()
                        ));
                    }
                    // cannot save custgame
                    else {
                        echo json_encode(array(
                            'ret_code' => 2,
                            'ret_msg' => "Something went wrong contact napiz"
                        ));
                    }
                }
                else {
                    echo json_encode(array(
                        'ret_code' => 1,
                        'ret_msg' => "Not Enough money"
                    ));
                }
            }
            else {
                echo json_encode(array(
                    'ret_code' => 2,
                    'ret_msg' => "Something went wrong contact napiz"
                ));
            }
        }else{
            echo json_encode(array(
                'ret_code' => -1,
                'ret_msg'  => "Post data required"
            ));

        }
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
