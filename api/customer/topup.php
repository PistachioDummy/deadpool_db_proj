<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/topup_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if (isset($_POST["money"])){
            $topup = Topup::newTopup($_SESSION["id"], $_POST["money"], time());
            TopupManager::createTopup($topup);
            if ($topup !== NULL) {
                //topup complete
                echo json_encode(array(
                    'ret_code' => 0,
                    'ret_msg' => "Topup successfully "
                ));
            }
            else {
                // cannot create topup object
                echo json_encode(array(
                    'ret_code' => 2,
                    'ret_msg' => 'Something wrong contact napiz'
                ));
            }
        }
        else {
            // post data not complete
            echo json_encode(array(
                'ret_msg' => "Post data required",
                'ret_code' => -1
            ));
        }
    }else{
        // no login
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
