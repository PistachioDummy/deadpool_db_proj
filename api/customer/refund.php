<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/offer.php';
    require_once ROOT . '/models/offer_manager.php';
    require_once ROOT . '/models/price.php';
    require_once ROOT . '/models/price_manager.php';
    require_once ROOT . '/models/refund.php';
    require_once ROOT . '/models/refund_manager.php';
    require_once ROOT . '/models/custgame_manager.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if(isset($_POST["game_id"])) {
            $offer = OfferManager::findOfferByCustIdAndGameId($_SESSION["id"],$_POST["game_id"])[0];
            $t = time();
            if($t <= strtotime($offer->getTime()) + 7*24*60*60) {
                $user = CustomerManager::findCustomerById($offer->getCustomerId());
                $price = PriceManager::findPriceById($offer->getPriceId());
                $user->setMoney($user->getMoney() + $price->getPrice());
                $user->save();
                CustGameManager::deleteCustGameById((int)$user->getId(),(int)$price->getGameId());
                $refund = Refund::newRefund($offer->getId(),date("Y-m-d H:m:s",$t));
                $refund->save();
                echo json_encode(array(
                    'ret_code' => 0,
                    'ret msg' => "queried"
                ));            
            }else {
                // can't refund
                echo json_encode(array(
                    'ret_code' => 1,
                    'ret_msg' => "time out for refund"
                ));
            }
/*            echo json_encode(array(
                'data' => (CustomerManager::findCustomerById($_SESSION["id"]))->toJSON(),
                'ret_msg' => "queried"
            ));*/
        }else {
            echo json_encode(array(
                'ret_code' => -1,
                'ret_msg' => "Post data required"
            ));
        }
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
