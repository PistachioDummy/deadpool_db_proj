<?php
    session_start();
    define(ROOT, $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . 'utility/function.php';
    require_once ROOT . 'models/customer_manager.php';
    require_once ROOT . 'models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if(isset($_POST["image_ref"])){
            $customer = CustomerManager::findCustomerById($_SESSION["id"]);
            $customer->setImageRef($_POST["image_ref"]);
            $customer.save();
            echo json_encode(array(
                'data' => $customer->getImagRef(),
                'ret_msg' => 'queried'
            ));
        }else{
            echo json_encode(array(
                'ret_code' => -1,
                'ret_msg'  => "Post data required"
            ));
        }
    }else{
        echo json_encode(array(
            'ret_code' => 0,
            "ret_msg" => "Login required"
        ));
    }
?>
