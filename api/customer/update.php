<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if(isset($_POST["profilename"]) || 
          isset($_POST["email"]) || isset($_POST["password"])){
              $customer = CustomerManager::findCustomerById($_SESSION["id"]);
              $name = isset($_POST["profilename"])?$_POST["profilename"]:NULL;
              $email = isset($_POST["email"])?$_POST["email"]:NULL;
              $password = isset($_POST["password"])?$_POST["password"]:NULL;
              if($name){
                $customer->setProfileName($name);
                $_SESSION["profilename"] = $name;
              }
              if($email){
                $customer->setEmail($email);
                $_SESSION["email"] = $email;
              }
              if($password){
                $customer->setPassword($password);
              }
              echo json_encode(array( 'ret_code' => $customer->save(), 'ret_msg' => 'Changed'));
        }else{
            echo json_encode(array(
                'ret_code' => -1,
                'ret_msg'  => "Post data required"
            ));

        }
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
