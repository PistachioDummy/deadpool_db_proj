<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if (isset($_POST["friend_id"]) && isset($_POST["status"])) {
            if ($_POST["status"] == 1) {
                $customer = CustomerManager::findCustomerById($_SESSION["id"]);
                $friend = CustomerManager::findCustomerById($_POST["friend_id"]);
                if ($customer->acceptFriend($friend)) {
                    echo json_encode(array('ret_code' => 0,
                        'ret_msg' => "Friend Accepted"
                    ));
                }
                else {
                    echo json_encode(array('ret_code' => 2,
                        'ret_msg' => "Something went wrong contact napiz"
                    ));
                }
            } else {
                $customer = CustomerManager::findCustomerById($_SESSION["id"]);
                $friend = CustomerManager::findCustomerById($_POST["friend_id"]);
                if ($customer->rejectFriend($friend)) {
                    echo json_encode(array('ret_code' => 0,
                        'ret_msg' => "Friend Rejected"
                    ));
                }
                else {
                    echo json_encode(array('ret_code' => 2,
                        'ret_msg' => "Something went wrong contact napiz"
                    ));
                }
            }
        }
        else {
            echo json_encode(array('ret_code' => -1,
                'ret_msg' => "Post data required"
            ));
        }
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
