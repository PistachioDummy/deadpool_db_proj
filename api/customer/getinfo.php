<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        echo json_encode(array(
            'data' => CustomerManager::findCustomerById($_SESSION["id"])->toArray(),
            'ret_msg' => "queried"
        ));
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
