<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if (isset($_POST["friend_username"])) {
            $customer = CustomerManager::findCustomerById($_SESSION["id"]);
            $friend = CustomerManager::findCustomerByUsername($_POST["friend_username"]);
            if ($friend) {
                if($friend->getId() === $customer->getId()){
                    echo json_encode(array(
                        'ret_code' => 0,
                        'ret_msg' => "Cannot add friend"
                    ));
                }
                else if ($customer->addFriend($friend)){
                    echo json_encode(array(
                        'ret_code' => 0,
                        'ret_msg' => "Added User {$friend->getProfileName()} "
                    ));
                }
                else {
                    echo json_encode(array(
                        'ret_code' => 2,
                        'ret_msg' => "something went wrong contact napiz"
                    ));
                }
            }
            else {
                echo json_encode(array(
                    'ret_code' => 1,
                    'ret_msg' => "User not founded"
                ));
            }
        }
        else {
            echo json_encode(array(
                'ret_code' => -1,
                'ret_msg' => "Post data required " . print_r($_POST, true)
            ));
        }
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
