<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["email"])){
        $customer = Customer::newCustomer($_POST["username"], $_POST["password"], $_POST["username"], 0, $_POST["email"], NULL);
        $ret_code = CustomerManager::createCustomer($customer);
        if($ret_code == 3){
            $msg = "User Exists";
        }else if($ret_code === 2 || $ret_code === 1 || $ret_code == 4){
            $msg = "Failed to create user";
        }else if($ret_code == 0){
            $msg = "User created";
        }else {
            $msg = "Something went wrong contact napiz";
        }
        echo json_encode(array('ret_code' => $ret_code, 'ret_msg' => $msg));
    }else{
        echo json_encode(array('ret_code' => 0, 'ret_msg' => 'Failed to create user'));
    };
    
?>
