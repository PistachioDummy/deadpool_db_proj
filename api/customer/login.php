<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    //header('Content-Type: application/json');
    $output = "";
    if(isset($_POST["username"]) && isset($_POST["password"])){
        $customer = CustomerManager::findCustomerByUserName($_POST["username"]);
        if($customer){
            $ret_code = $customer->checkPassword($_POST["password"]);
            if($ret_code == true){
                $_SESSION["id"] = $customer->getId();
                $_SESSION["profilename"] = $customer->getProfileName();
                $_SESSION["username"] = $customer->getUsername();
                $_SESSION["email"] = $customer->getEmail();
                $_SESSION["money"] = $customer->getMoney();
                $_SESSION["logged_in"] = True;
                $msg = "login success";
            }else{
                $_SESSION["logged_in"] = False;
                $msg = "Wrong password";
            }
            $output = json_encode(array('ret_code' => $ret_code, 'ret_msg' => $msg));
        }else{
            $output = json_encode(array('ret_code' => 0, 'ret_msg' => "User not founded"));
        }
    }else{
        $output = json_encode(array('ret_code' => 0, 'ret_msg' => "Argument doesn't set"));
    }
    print $output;
?>
