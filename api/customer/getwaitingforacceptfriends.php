<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/customer_manager.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        $customer = CustomerManager::findCustomerById($_SESSION["id"]);
        $friends = $customer->getWaitingForAcceptFriends();
        $friends_json = array();
        foreach($friends as $user) {
            $friends_json[] = $user->toArray();
        }
        echo json_encode(array(
            'data' => $friends_json,
            'ret_msg' => "queried"
        ));
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
