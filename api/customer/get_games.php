<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    //header('Content-Type: application/json');
    $output = "";
    if(isLoggedIn() && isset($_SESSION["id"])){
        $customer = CustomerManager::findCustomerById($_POST["id"]);
        if($customer){
            $games = $customer->getGames();
            $games_array = array();
            foreach($games as $game){
                $games_array[] = $game->toArray();
            }
            $output = json_encode(array('ret_code' => 1, 'ret_msg' => "queried", 'data' => $games_array));
        }else{
            $output = json_encode(array('ret_code' => 0, 'ret_msg' => "User not founded", 'data' => NULL));
        }
    }else{
        $output = json_encode(array('ret_code' => 0, 'ret_msg' => "Argument doesn't set", 'data' => NULL));
    }
    print $output;
?>
