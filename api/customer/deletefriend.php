<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        if (isset($_POST["friend_id"])) {
            $customer = CustomerManager::findCustomerById($_SESSION["id"]);
            $friend = CustomerManager::findCustomerById($_POST["friend_id"]);
            if ($friend && $customer) {
                if ($customer->deleteFriend($friend) || $friend->deleteFriend($customer) ){
                    echo json_encode(array(
                        'ret_code' => 0,
                        'ret_msg' => "deleted from friend list"
                    ));
                }
                else {
                    echo json_encode(array(
                        'ret_code' => 2,
                        'ret_msg' => "something went wrong contact napiz"
                    ));
                }
            }
            else {
                echo json_encode(array(
                    'ret_code' => 1,
                    'ret_msg' => "invalid friend id"
                ));
            }
        }
        else {
            echo json_encode(array(
                'ret_code' => -1,
                'ret_msg' => "Post data required"
            ));
        }
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }
?>
