<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/developer_manager.php';
    require_once ROOT . '/models/developer.php';
    header('Content-Type: application/json');
    if(isset($_POST["name"]) && isset($_POST["phonenumber"]) && isset($_POST["city"])){
        $dev = Developer::newDeveloper($_POST["name"], $_POST["phonenumber"], $_POST["city"]);
        $ret_code = DeveloperManager::createDeveloper($dev);
        if($ret_code == 3){
            $msg = "Developer Exists";
        }else if($ret_code == 2 || $ret_code == 1 || $ret_code == 4){
            $msg = "Failed to create developer";
        }else if($ret_code == 0){
            $msg = "Developer created";
        }else {
            $msg = "Something went wrong contact napiz";
        }
        echo json_encode(array('ret_code' => $ret_code, 'ret_msg' => $msg));
    }else{
        echo json_encode(array('ret_code' => 0, 'ret_msg' => 'Failed to create user'));
    };
    
?>
