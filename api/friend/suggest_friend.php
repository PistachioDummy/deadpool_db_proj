<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if(isLoggedIn()){
        $cust_id = $_POST["cust_id"];
        if(isset($_POST["cust_id"])){
            $mutual_friend_list = CustomerManager::findSuggestFriendById($cust_id);
            echo json_encode(array(
                'data' => $mutual_friend_list,
                'ret_msg' => "Top 10 mutual friends"
            ));
        }else{
            echo json_encode(array(
                'ret_code' => -1,
                'ret_msg'  => "Post data required"
            ));
        }
    }else{
        echo json_encode(array('ret_code' => 0 ,
            "ret_msg" => "Login required"
        ));
    }

?>
