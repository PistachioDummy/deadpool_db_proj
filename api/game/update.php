<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/game_manager.php';
    require_once ROOT . '/models/game.php';
    require_once ROOT . '/models/developer_manager.php';
    header('Content-Type: application/json');
    if((isset($_POST["name"]) || isset($_POST["dev_id"])) && isset($_POST["game_id"])){
          $game_name = isset($_POST["game_name"])?$_POST["game_name"]:NULL;
          $dev_id = isset($_POST["email"])?$_POST["email"]:NULL;
          $game = GameManager::findGameById($_POST["game_id"]);
          if($game_name){
            $game->setName($game_name);
          }
          if ($dev_id) {
            $game->setDeveloperId($dev_id);
          }
          $game.save();
          echo json_encode(array(
            'ret_code' => 0,
            'ret_msg' => "Queried"
        ));
    }else{
        echo json_encode(array(
            'ret_code' => -1,
            'ret_msg'  => "Post data required"
        ));
    }
?>
