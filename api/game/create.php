<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/developer.php';
    require_once ROOT . '/models/game.php';
    header('Content-Type: application/json');
    if(isset($_POST["name"]) && isset($_POST["dev_id"]) && isset($_POST["dlc_status"])){
        $game = Game::newGame($_POST["name"], $_POST["dev_id"], $_POST["dlc_status"]);
        $ret_code = GameManager::createGame($game);
        if($ret_code == 3){
            $msg = "Game Exists";
        }else if($ret_code === 2 || $ret_code === 1 || $ret_code == 4){
            $msg = "Failed to create game";
        }else if($ret_code == 0){
            $msg = "Game created";
        }else {
            $msg = "Something went wrong contact napiz";
        }
        echo json_encode(array('ret_code' => $ret_code, 'ret_msg' => $msg));
    }else{
        echo json_encode(array('ret_code' => 0, 'ret_msg' => 'Failed to create user'));
    };
    
?>
