<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if (isset($_POST["id"])) {
        echo json_encode(array(
            'ret_code' => GameManager::deleteGameById($_POST["id"]),
            'ret_msg'  => "Delete Successful"
        ));
    }
    else {
        echo json_encode(array(
            'ret_code' => -1,
            'ret_msg' => "POST data required"
        ));
    }
?>
