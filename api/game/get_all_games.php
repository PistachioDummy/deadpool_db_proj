<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/game_manager.php';
    require_once ROOT . '/models/game.php';
    header('Content-Type: application/json');
    if (isLoggedIn()) {
        $games = GameManager::findAllWithOutDLC();
        if(!$games){
            echo json_encode(array(
                'data' => NULL,
                'ret_msg' => 'Error Contact Admin',
                'ret_code' => -1
            ));
        }else{
            $games_json = array();
            foreach($games as $game){
                $games_json[] = $game->toArray();
            }
            echo json_encode(array(
                'data' => $games_json,
                'ret_msg' => 'queried',
                'ret_code' => 1
            ));
        }
            
    }
    else {
        echo json_encode(array(
            'ret_code' => -2,
            'ret_msg' => "Login Required"
        ));
    }
?>
