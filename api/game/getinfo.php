<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    header('Content-Type: application/json');
    if (isset($_POST["id"]) && isLoggedIn()) {
        echo json_encode(array(
            'data' => GameManager::findGameById($_POST["id"])->toJSON(),
            'ret_msg' => "queried"
        ));
    }
    else {
        echo json_encode(array(
            'ret_code' => -1,
            'ret_msg' => "POST data required"
        ));
    }
?>
