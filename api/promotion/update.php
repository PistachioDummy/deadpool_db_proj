<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/promotion_manager.php';
    require_once ROOT . '/models/promotion.php';
    header('Content-Type: application/json');
    if((isset($_POST["name"]) ||
      isset($_POST["start"]) || isset($_POST["end"])) && (isset($_POST["promotion_id"]))){
        $promotion = PromotionManager::findPromotionById($_POST["promotion_id"]);
          $name = isset($_POST["name"])?$_POST["name"]:NULL;
          $start = isset($_POST["start"])?$_POST["start"]:NULL;
          $end = isset($_POST["end"])?$_POST["end"]:NULL;
          if($name){
            $promotion->setName($name);
          }
          if($start){
            $promotion->setStartDate($start);
          }
          if($end){
            $promotion->setEndDate($end);
          }
          $promotion.save();
            echo json_encode(array(
                'ret_code' => 0,
                'ret_msg' => "queried"
            ));
    }else{
        echo json_encode(array(
            'ret_code' => -1,
            'ret_msg'  => "Post data required"
        ));
    }
?>
