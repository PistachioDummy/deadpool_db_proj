<?php
    session_start();
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/utility/function.php';
    require_once ROOT . '/models/promotion_manager.php';
    require_once ROOT . '/models/promotion.php';
    header('Content-Type: application/json');
    if(isset($_POST["name"]) && isset($_POST["start"]) && isset($_POST["end"]) ){
        $promotion = Promotion::newPromotion($_POST["name"], $_POST["start"], $_POST["end"]);
        $ret_code = PromotionManager::createPromotion($promotion);
        if($ret_code == 3){
            $msg = "Promotion Exists";
        }else if($ret_code === 2 || $ret_code === 1 || $ret_code == 4){
            $msg = "Failed to create promotion";
        }else if($ret_code == 0){
            $msg = "Promotion created";
        }else {
            $msg = "Something went wrong contact napiz";
        }
        echo json_encode(array('ret_code' => $ret_code, 'ret_msg' => $msg));
    }else{
        echo json_encode(array('ret_code' => 0, 'ret_msg' => 'Failed to create user'));
    };
    
?>
