<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/customer_sql.php';
    require_once ROOT . '/sql/friend_sql.php';
    require_once ROOT . '/sql/game_sql.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/custgame_manager.php';
    require_once ROOT . '/models/game_manager.php';
    class Customer {
        private $id;
        private $username;
        private $password;
        private $profilename;
        private $money;
        private $email;
        private $image_ref;

        // Lazy initialize
        private $friends;
        private $games;
        private $pending_friends;
        private $wating_for_accept_friends;

        private function isValidCustomer(){
            if($this->getProfileName() === NULL || $this->getId() === NULL || $this->getUsername() === NULL || $this->getPassword() === NULL ||
                $this->getMoney() === NULL || $this->getEmail() === NULL ) return false;
            return true;
        }
        public function __construct(){}
        
        public static function loadCustomer($id, $username, $password, $profilename, $money, $email, $image_ref){
            $instance = new self();
            $instance->setId($id);
            $instance->setUsername($username);
            $instance->setProfileName($profilename);
            /* please don't fucking use 
                $instance->setPassword($password);
                or it will bug and i need to fix it again :<
             */
            $instance->password = $password;
            $instance->setMoney($money);
            $instance->setEmail($email);
            $instance->setImageRef($image_ref);
            return $instance;
        }
        
        // php doesn't allow overload constructor :<
        public static function newCustomer($username, $password, $profilename, $money, $email, $image_ref){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setUsername($username);
            $instance->setPassword($password);
            $instance->setProfileName($profilename);
            $instance->setMoney($money);
            $instance->setEmail($email);
            $instance->setImageRef($image_ref);
            return $instance;
        }

        // change primary key value is not permitted
        public function getId(){
            return $this->id;
        }

        public function setProfileName($profilename){
            $this->profilename = $profilename;
        }
        public function getProfileName(){
            return $this->profilename;
        }

        public function getUsername(){
            return $this->username;
        }
        private function setUsername($name) {
            $this->username = $name;
        }
        private function setId($id) {
            $this->id = $id;
        }

        public function getMoney(){
            return $this->money;
        }

        public function setMoney($money){
            return $this->money = $money;
        }

        public function getEmail(){
            return $this->email;
        }
        public function setEmail($email){
            return $this->email = $email;
        }
        public function setImageRef($image_ref) {
            return $this->image_ref = $image_ref;
        }
        public function getImageRef() {
            return $this->image_ref;
        }
        public function getFriends(){
            if($this->friends === null){
                $friends = array();
                $friends_id = FriendSQL::findAcceptedFriendsIdByCustId($this->getId());
                foreach($friends_id as $id) {
                    $friends[] = CustomerManager::findCustomerById($id);
                }
                $this->friends = $friends;
            }
            return $this->friends;
        }
        public function getWaitingForAcceptFriends() {
            if($this->friends === null){
                $friends = array();
                $friends_id = FriendSQL::findWaitingForAcceptFriendIdByCustId($this->getId());
                foreach($friends_id as $id) {
                    $friends[] = CustomerManager::findCustomerById($id);
                }
                $this->waiting_for_accept_friends = $friends;
            }
            return $this->waiting_for_accept_friends;

        }
        public function getPendingFriends() {
            if ($this->pending_friends === null) {
                $friends = array();
                $friends_id = FriendSQL::findPendingFriendIdByCustId($this->getId());
                foreach($friends_id as $id) {
                    $friends[] = CustomerManager::findCustomerById($id);
                }
                $this->pending_friends = $friends;
            }
            return $this->pending_friends;
        }
        public function getGames(){
            //if($this->games === null){
                $this->games = array();
                $listAll = CustGameManager::findCustGameByCustomerId($this->id);
                foreach($listAll as $game) {
                    if(GameManager::findDLCByGameId($game->getGameId()) === NULL) {
                        $this->games[] = GameSQL::findGameById($game->getGameId());
                    }
                }
            //}
            return $this->games;
        }

        public function checkPassword($password){
            return password_verify($password, $this->getPassword());;
        }
        // Don't use this if not neccessary
        public function getPassword(){
            return $this->password;
        }
        public function setPassword($password){
            $this->password = password_hash($password, PASSWORD_BCRYPT);
        }
        
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'username' => $this->getUsername(),
                'money' => $this->getMoney(),
                'profilename' => $this->getProfileName(),
                'email' => $this->getEmail(),
                'friends' => $this->getFriends(),
                'games' => $this->getGames(),
                'image_ref' => $this->getImageRef()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
        public function save(){
            if($this->getId() === NULL) {
                return False;
            }
            if($this->isValidCustomer() === false) 
                return False;
            if(CustomerManager::findCustomerById($this->getId()) === NULL)
                return False;
            return CustomerSQL::updateCustomer($this);
        }


        public function addFriend($customer){
            if($this->isValidCustomer() === false || $customer->isValidCustomer() === false) return false;
            if(FriendSQL::isRowExistsByPk($this->getId(), $customer->getId()) || FriendSQL::isRowExistsByPk($customer->getId(), $this->getId())){
                return True;
            }
            return FriendSQL::createFriend($this, $customer);
        }

        public function acceptFriend($customer){
            if($this->isValidCustomer() === false || $customer->isValidCustomer() === false) 
                return False;
            if(FriendSQL::isRowExistsByPk($customer->getId(), $this->getId())){
                return FriendSQL::acceptFriendStatus($customer, $this);
            }
            return False;
        }

        public function rejectFriend($customer){
            if($this->isValidCustomer() === false || $customer->isValidCustomer() === false) 
                return False;
            if(FriendSQL::isRowExistsByPk($customer->getId(), $this->getId())){
                return FriendSQL::deleteFriendStatus($customer, $this);
            }
            return False;
        }

        public function deleteFriend($customer){
            if($this->isValidCustomer() === false || $customer->isValidCustomer() === false) 
                return False;
            if(FriendSQL::isRowExistsByPk($this->getId(), $customer->getId())){
                return FriendSQL::deleteFriendStatus($this, $customer);
            }
            return False;
        }

        public function toJSON($excludes=array("games", "friends", "pending_friends", "waiting_for_accept_friends")){
            return json_encode($this->toArray($excludes));
        }
    };
?>
