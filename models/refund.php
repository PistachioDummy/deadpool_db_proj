<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/models/offer_manager.php';
    require_once ROOT . '/sql/offer_sql.php';
    require_once ROOT . '/sql/refund_sql.php';
    require_once ROOT . '/models/refund_manager.php';
    class Refund {
        private $id;
        private $offer_id;
        private $time;
            
        //lazy init
        private $offer;
        public function __construct(){}


        private function isValidRefund(){
            if($this->getId() === NULL || $this->getOfferId() === NULL || $this->getTime() === NULL)
                return false;
            return true;
        }

        private function loadRefund($id, $offer_id, $time){
            $instance = new self();
            $instance->setId($id);
            $instance->setOfferId($offer_id);
            $instance->setTime($time);
            return $instance;
        }

        private function newRefund($offer_id, $time){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setOfferId($offer_id);
            $instance->setTime($time);
            return $instance;
        }

        public function save() {
            if($this->getId() === NULL)
                return False;
            if($this->isValidRefund() === False) 
                return False;
            if(RefundManager::findRefundById($this->getId()) === NULL)
                return False;
            return RefundSQL::updateRefund($this);
        }

        private function setId($id){
            $this->id = $id;
        }
        private function setOfferId($offer_id) {
            $this->offer_id = $offer_id;
            $this->setOffer(NULL);
        }
        private function setTime($time){
            $this->time = $time;
        }

        public function getId(){
            return $this->id;
        }
        public function getOffer(){
            if ($this->offer === NULL) {
                $this->offer = OfferManager::findOfferById($this->getOfferId());
            }
            return $this->offer;
        }
        public function getOfferId() {
            return $this->offer_id;
        }
        public function getTime(){
            return $this->time;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'offer_id' => $this->getOfferId(),
                'time' => $this->getTime()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
    };
?>
