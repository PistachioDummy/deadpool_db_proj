<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/game.php';
    class CustGame{
        private $cust_id;
        private $game_id;
        private $play_time;

        //lazy init
        private $game;
        private $customer;

        private function isValidCustGame(){
            if($this->getCustomerId() === NULL || $this->getGameId() === NULL || $this->getPlayTime() === NULL)
                return false;
            return true;
        }

        public function __construct(){}

        public static function loadCustGame($cust_id, $game_id, $play_time){
            $instance = new self();
            $instance->setCustomerId($cust_id);
            $instance->setGameId($game_id);
            $instance->setPlayTime($play_time);
            return $instance;
        }
        public static function newCustGame($cust_id, $game_id, $play_time){
            $instance = new self();
            $instance->setCustomerId($cust_id);
            $instance->setGameId($game_id);
            $instance->setPlayTime($play_time);
            return $instance;
        }
        public function save() {
            if ($this->getCustomerId() === NULL || $this->getGameId() === NULL) {
                return False;
            }
            else {
                return CustGameSQL::updateCustGame($this);
            }
        }

        //set
        private function setCustomerId($id){
            $this->cust_id = $id;
            $this->customer = NULL;
        }
        private function setGameId($game_id) {
            $this->game_id = $game_id;
            $this->game = NULL;
        }
        public function setPlayTime($play_time){
            $this->play_time = $play_time;
        }

        //get
        public function getCustomerId(){
            return $this->cust_id;
        }
        public function getCustomer() {
            if ($this->customer === NULL) {
                $this->customer = CustomerManager::findCustomerById($this->getCustomerId());
            }
            return $this->customer;
        }
        public function getGameId(){
            return $this->game_id;
        }
        public function getGame() {
            if ($this->game === NULL) {
                $this->game = GameManager::findGameById($this->getGameId());
            }
            return $this->game;
        }
        public function getPlayTime(){
            return $this->play_time;
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'cust_id' => $this->getCustomerId(),
                'game_id' => $this->getGameId(),
                'play_time' => $this->getPlayTime()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
    }
?>
