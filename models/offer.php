<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/customer_sql.php';
    require_once ROOT . '/sql/friend_sql.php';
    require_once ROOT . '/models/customer_manager.php';
    
    class Offer {
        private $id;
        private $cust_id;
        private $price_id;
        private $time;
        
        //lazy init
        private $customer;
        private $price;
        private function isValidOffer(){
            if($this->getId() === NULL || $this->getCustomerId() === NULL || $this->getPriceId() === NULL || $this->getTime() === NULL)
                return false;
            return true;
        }

        public function __construct(){}

        public static function loadOffer($id, $customer_id, $price_id, $time){
            $instance = new self();
            $instance->setId($id);
            $instance->setCustomerId($customer_id);
            $instance->setPriceId($price_id);
            $instance->setTime($time);
            return $instance;
        }
        public static function newOffer($customer_id, $price_id, $time){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setCustomerId($customer_id);
            $instance->setPriceId($price_id);
            $instance->setTime($time);
            return $instance;
        }

        public function save(){
            if($this->getId() === NULL)
                return False;
            if($this->isValidOffer() === False) 
                return False;
            if(OfferManager::findOfferById($this->getId()) === NULL)
                return False;
            return OfferSQL::updateOffer($this);
        }
        
        //set
        private function setId($id){
            $this->id = $id;
        }
        public function setCustomerId($id) {
            $this->cust_id = $id;
            $this->customer = NULL;
        }
        public function setPriceId($id) {
            $this->price_id = $id;
            $this->price = NULL;
        }
        public function setCustomer($cust) {
            $this->customer = $cust;
            $this->cust_id = $cust->getId();
        }
        public function setPrice($price) {
            $this->price = $price;
            $this->price_id = $price->getId();
        }
        public function setTime($time){
            $this->time = $time;
        }

        //get
        public function getId(){
            return $this->id;
        }
        public function getCustomer(){
            if ($this->customer === NULL) {
                $this->customer = CustomerManager::findCustomerById($this->getCustomerId());
            }
            return $this->customer;
        }
        public function getPrice(){
            if ($this->price === NULL) {
                $this->price = PriceManager::findPriceById($this->getPriceId());
            }
            return $this->price;
        }
        public function getPriceId() {
            return $this->price_id;
        }
        public function getCustomerId() {
            return $this->cust_id;
        }
        public function getTime(){
            return $this->time;
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'cust_id' => $this->getCustomerId(),
                'price_id' => $this->getPriceId(),
                'time' => $this->getTime()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
    }
?>
