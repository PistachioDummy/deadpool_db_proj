<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/friend_sql.php';
    require_once ROOT . '/models/customer_manager.php';
    class Friend {
        private $cust_id;
        private $friend_id;
        private $status;

        //lazy init
        private $customer;
        private $friend;

        private function isValidFriend(){
            if($this->getCusomertId() === NULL || $this->getFriendId() === NULL || $this->getStatus() === NULL)
                return false;
            return true;
        }

        public function __construct(){}
        public static function loadFriend($cust_id, $friend_id, $status){
            $instance = new self();
            $instance->setCustomerId($cust_id);
            $instance->setFriendId($friend_id);
            $instance->setStatus($status);
            return $instance;
        }
        
        //set
        private function setCustomerId($id){
            $this->cust_id = $id;
            $this->customer = NULL;
        }
        private function setFriendId($id) {
            $this->friend_id = $id;
            $this->friend = NULL;
        }
        public function setStatus($status){
            $this->status = $status;
        }

        //get
        public function getCustomerId(){
            return $this->cust_id;
        }
        public function getFriendId() {
            return $this->friend_id;
        }
        public function getCustomer(){
            if ($this->customer === NULL) {
                $this->customer = CustomerManager::findCustomerById($this->getCustomerId());
            }
            return $this->customer;
        }
        public function getFriend() {
            if ($this->friend === NULL) {
                $this->friend = CustomerManager::findCustomerById($this->getFriendId());
            }
            return $this->friend;
        }
        public function getStatus(){
            return $this->status;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'cust_id' => $this->getCustomerId(),
                'friend_id' => $this->getFriendId(),
                'status' => $this->getStatus()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
    };
?>
