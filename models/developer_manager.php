<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/sql/developer_sql.php';
    require_once ROOT . '/models/developer.php';
    class DeveloperManager {
        // return true on success
        public static function createDeveloper($dev_object){
            // id is automatically assign by sql
            if($dev_object->getId() !== NULL) {
                return 1;
            }
            if ($dev_object->getName() === NULL || $dev_object->getCity() === NULL || $dev_object->getPhonenumber() === NULL) {
                return 2;
            }
            // Dev Exists
            if(DeveloperManager::findDeveloperByName($dev_object->getName())){
                return 3;
            }
            if(DeveloperSQL::createDeveloper($dev_object)){
                return 0;
            }
            return 4;
        }
        public static function deleteDeveloper($dev){
            if(DeveloperManager::findDeveloper($dev) == NULL)
                return False;
            return DeveloperSQL::deleteDeveloperById($dev->getId());
        }
        public static function deleteDeveloperById($id){
            return DeveloperSQL::deleteDeveloperById($id);
        }

        public static function findDeveloperById($id){
            return DeveloperSQL::findDeveloperById($id);
        }
        public static function findDeveloper($dev) {
            if (!$dev) {
                return False;
            }
            return DeveloperSQL::findDeveloperById($dev->getId());
        }

        public static function findDeveloperByPhonenumber($phone) {
            return DeveloperSQL::findDeveloperByPhonenumber($phone);
        }
        
        public static function findDeveloperByCity($city) {
            return DeveloperSQL::findDeveloperByCity($city);
        }
        public static function findDeveloperByName($name){
            return DeveloperSQL::findDeveloperByName($name);
        }
        public static function findAll(){
            return DeveloperSQL::findAll();
        }
    };

?>
