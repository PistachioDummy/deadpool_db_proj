<?php

    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/offer_sql.php';
    require_once ROOT . '/models/offer.php';
    require_once ROOT . '/models/price.php';
    require_once ROOT . '/models/price_manager.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/customer_manager.php';

    class OfferManager {
        // return true on success
        public static function createOffer($offer_object){
            // id is automatically assign by sql
            if($offer_object->getId() !== NULL){
                return 1;
            }
            if($offer_object->getCustomerId() === NULL || $offer_object->getPriceId() === NULL || $offer_object->getTime() === NULL) {
                return 2;
            }
            // User Exists
            if(OfferSQL::createOffer($offer_object)){
                return 0;
            }
            return 4;
        }
        public static function deleteOffer($offer){
            if(OfferManager::findOffer($offer) == NULL)
                return False;
            return OfferManager::deleteOfferById($offer->getId());
        }
        public static function deleteOfferById($id){
            return OfferSQL::deleteOfferById($id);
        }

        public static function findOfferById($id){
            return OfferSQL::findOfferById($id);
        }

        public static function findOfferByCustUsername($name) {
            return OfferSQL::findOfferByCustUsername($name);
        }

        public static function findOfferByCustId($id) {
            return OfferSQL::findOfferByCustId($id);
        }

        public static function findOfferByCustIdAndGameId($cust_id,$game_id) {
            return OfferSQL::findOfferByCustIdAndGameId($cust_id,$game_id);
        }

        public static function findOfferByPrice($price) {
            return OfferSQL::findOfferByPrice($price);
        }

        public static function findAll(){
            $price = PriceSQL::findAll();
            return $price;
        }
    };

?>
