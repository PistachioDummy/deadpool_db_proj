<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/custgame_sql.php';
    require_once ROOT . '/models/custgame.php';
    class CustGameManager {
        // return true on success
        public static function createCustGame($custgame_object){
            // if have not complete data
            if ($custgame_object->getCustomerId() === NULL || $custgame_object->getGameId() === NULL) {
                return 2;
            }
            // CustGame Exists
            if (CustGameManager::findCustGame($custgame_object->getCustomerId(), $custgame_object->getGameId())){
                return 3;
            }
            if(CustGameSQL::createCustomer($custgame_object)){
                $customer = CustomerManager::findCustomerById($custgame_object->getCustomerId());
                $gameprice = PriceManager::findPriceByGameId($custgame_object->getGameId());
                // Money not enough
                if ($customer->getMoney() < $game->getPrice()) {
                    return 5;
                }
                // Buy Game Complete
                else {
                    $customer->setMoney($customer->getMoney()-$game->getPrice());
                    return 0;
                }
            }
            return 4;
        }
        public static function deleteCustGame($custgame) {
            return CustGameManager::deleteCustGameById($custgame->getCustomerId(), $custgame->getGameId());
        }
        public static function deleteCustGameById($cust_id, $game_id){
            if(CustGameManager::findCustGame($cust_id, $game_id) == NULL)
                return False;
            return CustGameSQL::deleteCustGame($cust_id, $game_id);
        }

        public static function findCustGameByCustomerId($id) {
            return CustGameSQL::findCustGameByCustomerId($id);
        }
        public static function findCustGameByGameId($id) {
            return CustGameSQL::findCustGameByGameId($id);
        }
        public static function findCustGame($customer_id, $game_id) {
            return CustGameSQL::findCustGame($customer_id, $game_id);
        }

        public static function findAll(){
            return CustGameSQL::findAll();
        }
    };

?>
