<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/promotion_sql.php';
    require_once ROOT . '/models/promotion.php';
    class PromotionManager {
        // return true on success
        public static function createPromotion($promotion_object){
            // id is automatically assign by sql
            if($promotion_object->getId() !== NULL){
                return 1;
            }
            if ($promotion_object->getName() === NULL || $promotion_object->getStartDate() === NULL || $promotion_object->getEndDate() === NULL) {
                return 2;
            }
            // if end date before start date
            if ($promotion_object->getStartDate() > $promotion_object->getEndDate()) {
                return 3;
            }
            // User Exists
            if(PromotionSQL::createPromotion($promotion_object)){
                return 0;
            }
            return 4;
        }
        public static function findActivePromotion() {
            return PromotionSQL::findActivePromotion();
        }
        public static function deletePromotion($promotion){
            if (PromotionManager::findPromotion($promotion) == NULL)
                return False;
            return PromotionSQL::deletePromotionById($customer->getId());
        }
        public static function findPromotion($promotion){
            return PromotionSQL::findPromotionById($promotion->getId());
        }
        public static function deletePromotionById($id){
            return PromotionSQL::deletePromotionById($id);
        }

        public static function findPromotionById($id){
            return PromotionSQL::findPromotionById($id);
        }

        public static function findPromotionByName($name){
            return PromotionSQL::findPromotionByName($name);
        }
        public static function findAll(){
            return PromotionSQL::findAll();
        }
    };

?>
