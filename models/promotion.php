<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/promotion_sql.php';
    require_once ROOT . '/models/promotion_manager.php';
    class Promotion {
        private $id;
        private $name;
        private $start_date;
        private $end_date;

        //lazy init
        // private $game;

        private function isValidPromotion(){
            if($this->getId() === NULL || $this->getName() === NULL || $this->getStartDate() === NULL || $this->getEndDate() === NULL)
                return false;
            if ($this->getStartDate() > $this->getEndDate()) {
                return false;
            }
            return true;
        }

        public function __construct(){}

        public static function loadPromotion($id, $name, $start_date, $end_date){
            $instance = new self();
            $instance->setId($id);
            $instance->setName($name);
            $instance->setStartDate($start_date);
            $instance->setEndDate($end_date);
            return $instance;
        }

        public static function newPromotion($name, $start_date, $end_date){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setName($name);
            $instance->setStartDate($start_date);
            $instance->setEndDate($end_date);
            return $instance;
        }
        public function save() {
            if($this->getId() === NULL)
                return False;
            if($this->isValidPromotion() === false) 
                return False;
            if(PromotionManager::findPromotionById($this->getId()) === NULL)
                return False;
            return PromotionSQL::updatePromotion($this);
        }
        
        //set
        private function setId($id){
            $this->id = $id;
        }
        public function setStartDate($start_date){
            $this->start_date = $start_date;
        }
        public function setEndDate($end_date){
            $this->end_date = $end_date;
        }
        public function setName($name) {
            $this->name = $name;
        }
        //public function setGame($game) {
        //    $this->game = $game;
        //    $this->game_id = $game->getId();
        //}
        //public function setGameId($game_id) {
        //    $this->game_id = $game_id;
        //    $this->game = NULL;
        //}

        //get
        public function getId(){
            return $this->id;
        }
        //public function getGame() {
        //    if ($this->game === NULL) {
        //        $this->game = GameSQL::findGameById($this->getGameId());
        //    }
        //    return $this->game;
        //}
        public function getName(){
            return $this->name;
        }
        public function getStartDate(){
            return $this->start_date;
        }
        public function getEndDate(){
            return $this->end_date;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'name' => $this->getName(),
                'start_date' => $this->getStartDate(),
                'end_date' => $this->getEndDate()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
    };
?>
