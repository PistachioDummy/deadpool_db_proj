<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/models/developer_manager.php';
    require_once ROOT . '/sql/developer_sql.php';
    require_once ROOT . '/sql/game_sql.php';
    require_once ROOT . '/models/game_manager.php';
    require_once ROOT . '/models/developer.php';
    class Game {
        private $id;
        private $name;
        private $dev_id;
        private $parent_game_id;
        private $image_ref;

        //lazy initialized
        private $dlc_list;
        private $developer;
        private $parent_game;

        private function isValidGame(){
            if($this->getId() === NULL || $this->getName() === NULL || $this->getDeveloper() === NULL)
                return false;
            return true;
        }

        public function __construct(){}

        public static function loadGame($id, $name, $dev_id, $parent_game_id, $game_image_ref) {
            $instance = new self();
            $instance->setId($id);
            $instance->setName($name);
            $instance->setDeveloperId($dev_id);
            $instance->setParentGameId($parent_game_id);
            $instance->setImageRef($game_image_ref);
            return $instance;
        }

        public static function newGame($name, $dev_id, $parent_game_id, $game_image_ref){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setName($name);
            $instance->setDeveloperId($dev_id);
            $instance->setParentGameId($parent_game_id);
            $instance->setImageRef($game_image_ref);
            return $instance;
        }
        
        public function save() {
            if($this->getId() === NULL)
                return False;
            if($this->isValidGame() === False) 
                return False;
            if(GameManager::findGameById($this->getId()) === NULL)
                return False;
            return GameSQL::updateGame($this);
        }

        private function setId($id){
            $this->id = $id;
        }
        public function setName($name){
            $this->name = $name;
        }
        public function setDeveloper($dev) {
            $this->develoer = $dev;
            $this->setDeveloperId($dev->getId());
        }
        public function setDeveloperId($id) {
            $this->dev_id = $id;
            $this->developer = NULL;
        }
        public function setParentGameId($id) {
            $this->parent_game_id = $id;
            $this->parent_game = NULL;
        }
        public function setImageRef($image_ref) {
            $this->image_ref = $image_ref;
        }

        public function getId(){
            return $this->id;
        }
        public function getName(){
            return $this->name;
        }
        public function getDeveloperId() {
            return $this->dev_id;
        }
        public function getDeveloper(){
            if ($this->developer === NULL) {
                return DeveloperManager::findDeveloperById($this->getDeveloperId());
            }
            return $this->developer;
        }
        public function getParentGameId() {
            return $this->parent_game_id;
        }
        public function getParentGame() {
            if (!$this->parent_game && $this->getParentGameId()) {
                $this->parent_game = GameManager::findGameById($this->getParentGameId());
            }
            return $this->parent_game;
        }
        public function getDLCList(){
            if(!$this->dlc_list){
           
                $this->dlc_list = GameSQL::findDLCByGameId($this->getId());
            }
            return $this->dlc_list;
        }
        public function getImageRef() {
            return $this->image_ref;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'name' => $this->getName(),
                'dev_id' => $this->getDeveloperId(),
                'dev_name' => $this->getDeveloper()->getName(),
                'parent_game_id' => $this->getParentGameId(),
                'image_ref' => $this->getImageRef()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
    };
?>
