<?php
    class Topup {
        private $id;
        private $cust_id;
        private $money;
        private $time;

        //lazy init
        private $customer;

        private function isValidTopup(){
            if($this->getId() === NULL || $this->getCustomerId() === NULL || $this->getMoney() === NULL || $this->getTime() === NULL)
                return false;
            return true;
        }

        public function __construct(){}

        public static function loadTopup($id, $customer_id, $money, $time){
            $instance = new self();
            $instance->setId($id);
            $instance->setCustomerId($customer_id);
            $instance->setMoney($money);
            $instance->setTime($time);
            return $instance;
        }

        public static function newTopup($customer_id, $money, $time){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setCustomerId($customer_id);
            $instance->setMoney($money);
            $instance->setTime($time);
            return $instance;
        }

        //set
        private function setId($id){
            $this->id = $id;
        }
        private function setTime($time){
            $this->time = $time;
        }
        private function setMoney($money){
            $this->money = $money;
        }
        private function setCustomerId($cust_id) {
            $this->cust_id = $cust_id;
        }

        //get
        public function getId(){
            return $this->id;
        }
        public function getCustomerId() {
            return $this->cust_id;
        }
        public function getCustomer(){
            if ($this->customer === NULL) {
                $this->customer = CustomerManager::findCustomerById($this->getCustomerId());
            }
            return $this->customer;
        }
        public function getMoney(){
            return $this->money;
        }
        public function getTime(){
            return $this->time;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'cust_id' => $this->getCustomerId(),
                'money' => $this->getMoney(),
                'time' => $this->getTime()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }

    };
?>
