<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/sql/topup_sql.php';
    require_once ROOT . '/models/topup.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/customer.php';
    class TopupManager {
        // return true on success
        public static function createTopup($topup_object){
            // id is automatically assign by sql
            // if topup object have id will rejected
            if($topup_object->getId() !== NULL){
                return 1;
            }
            // if the topup object have not complete the component will be eerror
            if($topup_object->getCustomerId() === NULL || $topup_object->getMoney() === NULL){
                return 2;
            }
            //topup with the money = 0 => error
            if ($topup_object->getMoney() === 0) {
                return 3;
            }
            if(TopupSQL::createTopup($topup_object)){
                $customer = CustomerManager::findCustomerById($topup_object->getCustomerId());
                //echo "<pre>";
                //print_r($topup_object);
                //echo "</pre>";
                if ($customer !== NULL) {
                    //complete
                    $customer->setMoney($customer->getMoney() + $topup_object->getMoney());
                    $customer->save();
                    return 0;
                }
                else {
                    // wrong user id
                    return 5;
                }
            }
            return 4;
        }
        public static function findTopup($topup) {
            return TopupSQL::findTopupById($topup->getId());
        }
        public static function deleteTopup($topup){
            if(TopupManager::findTopup($topup) === NULL)
                return False;
            return TopupSQL::deleteTopupById($topup->getId());
        }
        public static function findTopupByCustomer($customer){
            return TopupSQL::findTopupByCustomerObject($customer);
        }
        public static function findTopupByUsername($username) {
            return TopupSQL::findTopupByUsername($username);
        }
        public static function findTopupByUserId($id) {
            return TopupSQL::findTopupByUserId($id);
        }
        public static function deleteTopupById($id){
            return TopupSQL::deleteTopupById($id);
        }
        public static function findTopupById($id){
            return TopupSQL::findTopupById($id);
        }

        public static function findAll(){
            return TopupSQL::findAll();
        }
    };

?>
