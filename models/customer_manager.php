<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/customer_sql.php';
    require_once ROOT . '/models/customer.php';
    class CustomerManager {
        // return true on success
        public static function createCustomer($customer_object){
            // id is automatically assign by sql
            if($customer_object->getId() !== NULL){
                return 1;
            }
            if($customer_object->getUsername() === NULL || $customer_object->getEmail() === NULL || 
                $customer_object->getProfileName() === NULL || $customer_object->getPassword() === NULL){
                return 2;
            }
            if($customer_object->getMoney() === NULL) $customer_object->setMoney(0);
            // User Exists
            if(CustomerManager::findCustomerByUsername($customer_object->getUsername())){
                return 3;
            }
            if(CustomerSQL::createCustomer($customer_object)){
                return 0;
            }
            return 4;
        }
        public static function deleteCustomer($customer){
            if(CustomerManager::findCustomer($customer) == NULL)
                return False;
            return CustomerSQL::deleteCustomerById($customer->getId());
        }
        public static function findCustomer($customer){
            return CustomerSQL::findCustomerById($customer->getId());
        }
        public static function deleteCustomerById($id){
            return CustomerSQL::deleteCustomerById($id);
        }

        public static function findCustomerById($id){
            return CustomerSQL::findCustomerById($id);
        }

        public static function findSuggestedFriendById($id){
            return CustomerSQL::findsuggestedFriendById($id);
        }

        public static function findCustomerByUsername($username){
            $customer = CustomerSQL::findCustomerByUsername($username);
            return $customer;
        }
        public static function findAll(){
            $customer = CustomerSQL::findAll();
            return $customer;
        }
        public static function findAllFriend(){
            return FriendSQL::findAll();
        }
    };

?>
