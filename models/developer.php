<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/sql/developer_sql.php';
    class Developer {
        private $id;
        private $name;
        private $phonenumber;
        private $city;

        private function isValidDeveloper(){
            if($this->getId() === NULL || $this->getName() === NULL || $this->getCity() === NULL || $this->getPhonenumber() === NULL) 
                return false;
            return true;
        }

        public function __construct(){}

        public static function loadDeveloper($id, $name, $phonenumber, $city){
            $instance = new self();
            $instance->setId($id);
            $instance->setName($name);
            $instance->setPhonenumber($phonenumber);
            $instance->setCity($city);
            return $instance;
        }

        public static function newDeveloper($name, $phonenumber, $city){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setName($name);
            $instance->setPhonenumber($phonenumber);
            $instance->setCity($city);
            return $instance;
        }
        public function save(){
            if($this->getId() === NULL)
                return False;
            if($this->isValidDeveloper() === false) 
                return False;
            if(DeveloperManager::findDeveloperById($this->getId()) === NULL)
                return False;
            return DeveloperSQL::updateDeveloper($this);
        }
        
        //set
        private function setId($id){
            $this->id = $id;
        }
        public function setName($name){
            $this->name = $name;
        }
        public function setPhonenumber($phone) {
            $this->phonenumber = $phone;
        }
        public function setCity($city){
            $this->city = $city;
        }

        //get
        public function getId(){
            return $this->id;
        }
        public function getName(){
            return $this->name;
        }
        public function getCity(){
            return $this->city;
        }
        public function getPhonenumber(){
            return $this->phonenumber;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'name' => $this->getName(),
                'phonenumber' => $this->getPhonenumber(),
                'city' => $this->getCity()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
    };
?>
