<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/price_sql.php';
    require_once ROOT . '/models/price.php';
    require_once ROOT . '/models/game.php';
    class PriceManager {
        // return true on success
        public static function createPrice($price_object){
            // id is automatically assign by sql
            if($price_object->getId() !== NULL){
                return 1;
            }
            if($price_object->getGameId() === NULL || $price_object->getPrice() === NULL) {
                return 2;
            }
            // User Exists
            if(PriceSQL::createPrice($price_object)){
                return 0;
            }
            return 4;
        }
        public static function deletePrice($price){
            if(PriceManager::findPrice($price) === NULL)
                return False;
            return PriceSQL::deletePriceById($price->getId());
        }
        public static function findPrice($price) {
            return PriceSQL::findPriceById($price->getId());
        }
        public static function deletePriceById($id){
            return PriceSQL::deletePriceById($id);
        }
        public static function findPriceByPromotionId($id) {
            return PriceSQL::findPriceByPromotionId($id);
        }
        public static function findPriceById($id){
            return PriceSQL::findPriceById($id);
        }
        public static function findPriceByGameId($id) {
            return PriceSQL::findPriceByGameId($id);
        }
        public static function findPriceByGame($game) {
            return PriceSQL::findPriceByGameId($game->getId());
        }
        public static function findPriceByPriceRange($from, $to) {
            if ($from > $to) return NULL;
            return PriceSQL::findByPriceRange($from, $to);
        }

        public static function findAll(){
            return PriceSQL::findAll();
        }
    };

?>
