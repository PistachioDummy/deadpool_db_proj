<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/refund_sql.php';
    require_once ROOT . '/models/refund.php';
    class RefundManager {
        // return true on success
        public static function createRefund($refund_object){
            // id is automatically assign by sql
            if($refund_object->getId() !== NULL) {
                return 1;
            }
            if ($refund_object->getOffer() === NULL || $refund_object->getTiem() === NULL) {
                return 2;
            }
            if(RefundSQL::createRefund($refund_object)){
                return 0;
            }
            return 4;
        }
        public static function deleteRefund($refund){
            if(RefundManager::findRefund($refund) === NULL)
                return False;
            return RefundSQL::deleteRefundById($refund->getId());
        }
        public static function findRefund($refund){
            return RefundSQL::findRefundById($refund->getId());
        }
        public static function findRefundById($id) {
            return RefundSQL::findRefundById($id);
        }
        public static function deleteRefundById($id){
            if (RefundManager::findRefundById($id) === NULL) {
                return False;
            }
            return RefundSQL::deleteRefundById($id);
        }

        public static function findRefundByOffer($offer) {
            return RefundSQL::findRefundByOffer($offer);
        }
        public static function findRefundByOfferId($id) {
            return RefundSQL::findRefundByOfferId($id);
        }

        public static function findRefundByTime($from, $to) {
            return RefundSQL::findRefundByTime($from,$to);
        }

        public static function findAll(){
            return RefundSQL::findAll();
        }
    };

?>
