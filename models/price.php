<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/price_sql.php';
    require_once ROOT . '/models/price_manager.php';
    class Price {
        private $id;
        private $promotion_id;
        private $game_id;
        private $price;

        //lazy init
        private $promotion;
        private $game;

        private function isValidPrice(){
            if($this->getId() === NULL || $this->getGameId() === NULL || $this->getPrice() === NULL)
                return false;
            return true;
        }

        public function __construct(){}

        public static function loadPrice($id, $promotion_id, $game_id, $game_price){
            $instance = new self();
            $instance->setId($id);
            $instance->setPromotionId($promotion_id);
            $instance->setGameId($game_id);
            $instance->setPrice($game_price);
            return $instance;
        }

        public static function newPrice($promotion_id, $game_id, $game_price){
            $instance = new self();
            $instance->setId(NULL);
            $instance->setPromotionId($promotion_id);
            $instance->setGameId($game_id);
            $instance->setPrice($game_price);
            return $instance;
        }
        public function save(){
            if($this->getId() === NULL) {
                return False;
            }
            if($this->isValidPrice() === false) 
                return False;
            if(PriceManager::findPriceById($this->getId()) === NULL)
                return False;
            return PriceSQL::updatePrice($this);
        }

        //set
        private function setId($id){
            $this->id = $id;
        }
        public function setPrice($game_price) {
            $this->price = $game_price;
        }
        public function setPromotionId($promo_id) {
            $this->promotion_id = $promo_id;
            $this->promotion = NULL;
        }
        public function setGameId($id) {
            $this->game_id = $id;
            $this->game = NULL;
        }
        
        //get
        public function getId() {
            return $this->id;
        }
        public function getPromotion(){
            if ($this->promotion === NULL) {
                $this->promotion = PromotionManager::findPromotionById($this->getPromotionId());
            }
            return $this->promotion;
        }
        public function getGameId() {
            return $this->game_id;
        }
        public function getGame(){
            if ($this->game === NULL) {
                $this->game = GameManager::findGameById($this->getGameId());
            }
            return $this->game;
        }
        public function getPrice() {
            return $this->price;
        }
        public function getPromotionId() {
            return $this->promotion_id;
        }
        public function toArray($excludes=array()){
            if(!is_array($excludes)) return NULL;
            // We always exclude password
            $data = array(
                'id' => $this->getId(),
                'promotion_id' => $this->getPromotionId(),
                'game_id' => $this->getGameId(),
                'price' => $this->getPrice()
            );
            // hack way :< bad coding
            foreach($excludes as $k => $exclude){
                if(array_key_exists($exclude, $data)){
                    unset($data[$exclude]);
                }
            }
            return $data;
        }
        public function toJSON($excludes=array()){
            return json_encode($this->toArray($excludes));
        }
    }
?>
