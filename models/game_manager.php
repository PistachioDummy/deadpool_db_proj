<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/sql/game_sql.php';
    require_once ROOT . '/models/game.php';
    require_once ROOT . '/models/developer.php';
    require_once ROOT . '/models/developer_manager.php';
    class GameManager {
        // return true on success
        public static function createGame($game_object){
            // id is automatically assign by sql
            if($game_object->getId() !== NULL){
                return 1;
            }
            if($game_object->getName() === NULL || $game_object->getDeveloper() === NULL) {
                return 2;
            }
            // User Exists
            if(GameSQL::createGame($game_object)){
                return 0;
            }
            return 4;
        }
        public static function deleteGame($game){
            if(GameManager::findGame($game) == NULL)
                return False;
            return GameSQL::deleteGameById($game->getId());
        }
        public static function findGame($game){
            return GameSQL::findGameById($game->getId());
        }
        public static function deleteGameById($id){
            return GameSQL::deleteGameById($id);
        }

        public static function findGameById($id){
            return GameSQL::findGameById($id);
        }
        public static function findDLCByGameId($id) {
            return GameSQL::findDLCByGameId($id);
        }
        public static function findGameByDeveloper($dev) {
            return GameSQL::findGameByDeveloper($dev);
        }

        public static function findGameByDeveloperId($dev_id) {
            return GameSQL::findGameByDeveloper(DeveloperManager::findDeveloperById($dev_id));
        }

        public static function findGameByName($name){
            return GameSQL::findGameByName($name);
        }
        public static function findAll(){
            return GameSQL::findAll();
        }
        public static function findAllWithOutDLC(){
            return GameSQL::findAllWithOutDLC();
        }

    };

?>
