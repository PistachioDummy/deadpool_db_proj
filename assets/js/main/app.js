(function() {
    var app = angular.module('mainApp', []);
    function setAndShowMessage(msg){
        if(msg != undefined){
            jQuery("#info-message").html(msg);
            jQuery("#inner-message").hide().show();
            setTimeout(function(){jQuery("#inner-message").hide() }, 2000);
        }
    }

    app.controller('MenuController', ['$http', '$scope', '$window', function($http, $scope, $window){
        var menu = this;
        $window.user = window.user;
        console.log($window.user);
        menu.menuList = {};
        $http.get(
            "/assets/json/menu.json",
            { withCredentials: true }
        ).then( function ( response ){
            data = response.data;
            menu.menuList = data;
        }, function (response ){
            setAndShowMessage("Something went wrong");
            $window.location.href = "/logout.php";
        });

         menu.currentMenu = menu.menuList[0];
         menu.viewUrl = "/form/blank.html";
         menu.isActive = function(id){
            return menu.currentMenu && menu.currentMenu.id == id;
        }
        
        menu.getUser = function(){ return $window.user };
        menu.logOut = function(){ $window.location.href = "/logout.php"; };

        menu.setSubMenuActive = function(id){
            menu.currentSubMenu = menu.currentMenu.subMenu[id];
            menu.viewUrl = menu.currentMenu.subMenu[id].formUrl + "?date=" + Date.now();
            console.log(menu.viewUrl);
        };

        menu.setSubMenu = function(id){
            $scope.updateTime = Date.now();
            menu.currentMenu = menu.menuList[id];
            menu.setSubMenuActive(0);
        };
        menu.isSubMenuActive = function(id){
            return menu.currentSubMenu && menu.currentSubMenu.id == id;
        };


    }]);

    app.controller('GameListController', ['$http', '$scope', function($http, $scope){
        var game = this;
        game.gameSearch = "";
        game.gameList = [];
        game.confirmId = 0;
        $http.get(
            "/api/game/get_all_games.php",
            { withCredentials: true }
        ).then( function ( response ){
            data = response.data;
            game.gameList = data.data;
        }, function (response){
            setAndShowMessage("Something went wrong");
        });

        game.getGame = function(){
            return game.gameList;
        }

        game.confirmBuyGame = function(id){
            game.confirmId = id;
        }
        game.buy = function(){

        }

    }]);

    app.controller('MyGameController', ['$http', '$scope', function($http, $scope){
        var game = this;
        game.gameSearch = "";
        game.gameList = [];
        game.confirmId = 0;
        $http.get(
            "/api/customer/get_games.php",
            { withCredentials: true }
        ).then( function ( response ){
            data = response.data;
            game.gameList = data.data;
        }, function (response){
            setAndShowMessage("Something went wrong");
        });

        game.getGame = function(){
            return game.gameList;
        }

        game.confirmRefundGame = function(id){
            game.confirmId = id;
        }
        game.buy = function(){

        }

    }]);

    app.controller('FriendListController', ["$http", "$scope", function($http, $scope){
        var friend = this;
        friend.friendList = [];
        friend.friendPendingList = [];
        friend.friendWaitingAcceptList = [];
        friend.confirmid = 0;
        friend.addFriendUsername = "";
        function loadFriendList(){
            $http.get(
                "/api/customer/getfriends.php",
                { withCredentials: true }
            ).then( function ( response ){
                data = response.data;
                friend.friendList = data.data;
            }, function (response){
                setAndShowMessage("Something went wrong friends");
            });

            $http.get(
                "/api/customer/getpendingfriends.php",
                { withCredentials: true }
            ).then( function (response){
                data = response.data;
                friend.friendPendingList = data.data;
            }, function(response){
                setAndShowMessage("Something went wrong pending");
            });

            $http.get(
                "/api/customer/getwaitingforacceptfriends.php",
                { withCredentials: true }
            ).then( function (response){
                data = response.data;
                friend.friendWaitingAcceptList = data.data;
            }, function(response){
                setAndShowMessage("Something went wrong");
            });
        }
        loadFriendList();
        friend.getFriends = function (){
            return friend.friendList;
        };

        friend.getPendingFriends = function(){
            console.log(friend.friendPendingList);
            return friend.friendPendingList;
        }
        friend.getWaitingFriends = function(){
            console.log(friend.friendWaitingAcceptList);
            return friend.friendWaitingAcceptList;
        }

        friend.confirmDeleteFriend = function (id){
            friend.confirmId = id;
        }
        friend.deleteFriend =  function (id){
            $http({
                method: "POST",
                url: "/api/customer/deletefriend.php", 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                config: { withCredentials: true },
                data: $.param({ friend_id: id })
            }).then( function success(response){
                var data = response.data;
                loadFriendList();
                setAndShowMessage(data.ret_msg);
            }, function failed(response){
                setAndShowMessage("User not found");
            });
        }
        
        friend.acceptOrRejectFriend = function(id, status){
            $http({
                method: "POST",
                url: "/api/customer/acceptrejectfriend.php", 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                config: { withCredentials: true },
                data: $.param({ friend_id: id, status: status})
            }).then( function success(response){
                var data = response.data;
                loadFriendList();
                setAndShowMessage(data.ret_msg);
            }, function failed(response){
                setAndShowMessage("User not found");
            });
        

        }

        friend.addFriend = function(){
            $http({
                method: "POST",
                url: "/api/customer/addfriend.php", 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                config: { withCredentials: true },
                data: $.param({ friend_username: friend.addFriendUsername
                })
            }).then( function success(response){
                var data = response.data;
                loadFriendList();
                setAndShowMessage(data.ret_msg);
            }, function failed(response){
                setAndShowMessage("User not found");
            });
        
        }
    }]);
    app.controller('TopupController', ['$http', "$scope", '$window', function ($http, $scope, $window){
        var topup = this;
        this.amount = 0;
        topup.topup = function(){
            $http({
                method: "POST",
                url: "/api/customer/topup.php", 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                config: { withCredentials: true },
                data: $.param({ money: topup.amount  })
            }).then( function success(response){
                var data = response.data;
                setAndShowMessage(data.ret_msg);
                $http.get(
                    "/api/customer/getinfo.php",
                    { withCredentials: true }
                ).then( function (response){
                    data = response.data;
                    $window.user = data.data;
                }, function(response){
                    setAndShowMessage("Something went wrong");
                });
            }, function failed(response){
                console.log(response);
                setAndShowMessage("Topup failed");
            });
            
        }
    }]);

    app.controller('editProfileController', ['$http', "$scope", '$window', function ($http, $scope, $window){
        var profile = this;
        profile.user = $window.user;
        profile.user.newPassword = "";
        profile.changeProfile = function(){
            $http({
                method: "POST",
                url: "/api/customer/update.php", 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                config: { withCredentials: true },
                data: $.param({ profilename: profile.user.profilename , email: profile.user.email })
            }).then( function success(response){
                var data = response.data;
                setAndShowMessage(data.ret_msg);
                    console.log(data);
                $http.get(
                    "/api/customer/getinfo.php",
                    { withCredentials: true }
                ).then( function (response){
                    data = response.data;
                    $window.user = data.data;
                }, function(response){
                    setAndShowMessage("Something went wrong");
                });
            }, function failed(response){
                setAndShowMessage("User not found");
            });
        }

        profile.changePassword = function(){
            $http({
                method: "POST",
                url: "/api/customer/update.php", 
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                config: { withCredentials: true },
                data: $.param({ password: profile.user.newPassword })
            }).then( function success(response){
                var data = response.data;
                setAndShowMessage(data.ret_msg);
                $http.get(
                    "/api/customer/getinfo.php",
                    { withCredentials: true }
                ).then( function (response){
                    data = response.data;
                    $window.user = data.data;
                }, function(response){
                    setAndShowMessage("Something went wrong");
                });
            }, function failed(response){
                setAndShowMessage("User not found");
            });
        
        }
    }]);

})();
