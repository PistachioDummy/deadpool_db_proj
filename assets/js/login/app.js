(function() {
    function setAndShowMessage(text){
        jQuery("#info-message").html(text);
        jQuery("#inner-message").hide().show();
    }
    var app = angular.module('loginApp', []);
    app.controller('loginFormController', ['$http', '$scope', '$window', function ($http, $scope, $window){
        var form = this;
        $scope.userRegex = "[a-zA-Z0-9]{5, 10}";
        $scope.passwordRegex = ".{5,30}";
        form.submit = function (isValid, event){
            var state = true;
            event.preventDefault();
            if( form.form.submit === 'Sign In' ){
                if(isValid){
                    $http({
                        url: "/api/customer/login.php", 
                        method: "POST",
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param({ 
                            username: form.form.username,
                            password: form.form.password
                        }),
                        config: { withCredentials: true }
                    }).then( function (response){
                            data = response.data;
                            console.log(data.ret_msg);
                            if(data.ret_code){
                                form.error_msg = "Redirecting to application..";
                                setAndShowMessage(form.error_msg);
                                $window.location.href = "/index.php";
                            }else{
                                form.error_msg = data.ret_msg;
                                setAndShowMessage(form.error_msg);
                                state = false;
                            }
                        }, function (response){
                            form.error_msg = "Server Error contact admin";
                            setAndShowMessage(form.error_msg);
                            state = false;
                        }
                    );
                }else{
                    form.error_msg = "Wrong username / password format";
                    setAndShowMessage(form.error_msg);
                    state = false;
                }
            }else if( form.form.submit === 'Register'){
                jQuery('#registerModal').modal('show');
            }
        };

        form.form = {
            username: '',
            password: '',
            submit: ''
        };
        
    }]);
    app.controller('registerFormController', ['$http', '$scope', function ($http, $scope){
        var form = this;
        form.form = {
            username: '',
            password: '',
            email: ''
        };
        form.clearData = function(){
            form.form = {
                username: '',
                password: '',
                email: ''
            };
        }
        form.register = function(){
            $http({
                method: "POST",
                url: "/api/customer/create.php", 
                config: { withCredentials: true },
                data: $.param({ 
                    username: form.form.username,
                    password: form.form.password,
                    email: form.form.email
                })
            }).then( function success(response){
                data = response.data;
                setAndShowMessage(data.ret_msg);
                jQuery('#registerModal').modal('hide');
                form.clearData();
            }, function failed(){
                jQuery('#registerModal').modal('hide');
                setAndShowMessage("Request failed contact admin");
                form.clearData();
            });
        }

    }]);
})();


