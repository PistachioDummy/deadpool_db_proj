<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/setup/customer_table.php';
    require_once ROOT . '/setup/custgame_table.php';
    require_once ROOT . '/setup/developer_table.php';
    require_once ROOT . '/setup/friend_table.php';
    require_once ROOT . '/setup/game_table.php';
    require_once ROOT . '/setup/offer_table.php';
    require_once ROOT . '/setup/price_table.php';
    require_once ROOT . '/setup/promotion_table.php';
    require_once ROOT . '/setup/refund_table.php';
    require_once ROOT . '/setup/topup_table.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/customer_manager.php';
    require_once ROOT . '/models/custgame.php';
    require_once ROOT . '/models/custgame_manager.php';
    require_once ROOT . '/models/developer.php';
    require_once ROOT . '/models/developer_manager.php';
    require_once ROOT . '/models/friend.php';
    require_once ROOT . '/models/game.php';
    require_once ROOT . '/models/game_manager.php';
    require_once ROOT . '/models/offer.php';
    require_once ROOT . '/models/offer_manager.php';
    require_once ROOT . '/models/price.php';
    require_once ROOT . '/models/price_manager.php';
    require_once ROOT . '/models/promotion.php';
    require_once ROOT . '/models/promotion_manager.php';
    require_once ROOT . '/models/refund.php';
    require_once ROOT . '/models/refund_manager.php';
    require_once ROOT . '/models/topup.php';
    require_once ROOT . '/models/topup_manager.php';

function testing($va){
    if($va){
        echo "True\n";
    }else{
        echo "False\n";
    }
}
/* Clean data */
deleteRefundTable();
deleteOfferTable();
deletePriceTable();
deletePromotionTable();
deleteTopupTable();
deleteFriendTable();
deleteCustGameTable();
deleteGameTable();
deleteDeveloperTable();
deleteCustomerTable();

createCustomerTable();
createDeveloperTable();
createGameTable();
createCustGameTable();
createFriendTable();
createTopupTable();
createPromotionTable();
createPriceTable();
createOfferTable();
createRefundTable();

$numberOfCustomers = 60;
$numberOfDevelopers = 10;
$numberOfGames = 30;
$numberOfDlc = 40;

/* Create data for CustomerTable */
foreach(range(1,$numberOfCustomers) as $customer){
    $customers[] = Customer::newCustomer("DeadPool".$customer ,"deadpool","DeadPool_".$customer, rand(0,100), "deadp00l".$customer."@deadpool.qwe", NULL);
}
foreach($customers as $customer){
    CustomerManager::createCustomer($customer);
}

/* Create data for DeveloperTable */
foreach(range(1,$numberOfDevelopers) as $developer) {
    $developers[] = Developer::newDeveloper("CaptainDeadPool".$developer, "0987654321", "deadp00lCity");
}
foreach($developers as $developer) {
    DeveloperManager::createDeveloper($developer);
}

/* Create data for GameTable */
foreach(range(1,$numberOfGames) as $game) {
    $games[] = Game::newGame("DeadPool ".$game,rand(1,$numberOfDevelopers),NULL,NULL);
}
foreach(range(1,$numberOfDlc) as $dlc) {
    $random_game = rand(0,$numberOfGames - 1);
    /* We can use $random_game as game id because we delete all table before add data so games id will equal $random_game */
    $games[] = Game::newGame($games[$random_game]->getName() . " DLC " . $dlc,$games[$random_game]->getDeveloperId(), $random_game ,NULL);
}
foreach($games as $game) {
    GameManager::createGame($game);
}

$games = GameSQL::findAll();

/* Random Price */
foreach(range(0,$numberOfGames - 1) as $game) {
    $prices[] = Price::newPrice(NULL,$games[$game]->getId(),rand(1,10)*100);
}
foreach(range($numberOfGames,$numberOfGames+$numberOfDlc-1) as $dlc) {
    $prices[] = Price::newPrice(NULL,$games[$dlc]->getId(),rand(3,5)*10);
}
foreach($prices as $price) {
    PriceManager::createPrice($price);
}

echo "\nDONE!?";

/*
deleteRefundTable();
deleteOfferTable();
deletePromotionTable();
deletePriceTable();
deleteTopupTable();
deleteFriendTable();
deleteCustomerGameTable();
deleteGameTable();
deleteDeveloperTable();
deleteCustomerTable();
*/
?>
