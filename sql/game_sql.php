<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/game.php';
    require_once ROOT . '/models/developer.php';
    class GameSQL {

        private static function rawGameToGame($raw_game){
            return Game::loadGame($raw_game["game_id"], $raw_game["game_name"], $raw_game["dev_id"], $raw_game["parent_game_id"], $raw_game["game_image_ref"]);
        }

        private static function rawGameListToGame($raw_game_list){
            $game_list = array();
            foreach($raw_game_list as $raw_game){
                $game_list[] = GameSQL::rawGameToGame($raw_game);
            }
            return $game_list;
        }

        public static function createGame($game){
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Game (game_name, dev_id, parent_game_id, game_image_ref) VALUES (:game_name, :dev_id, :parent, :image_ref)");
            $row_count = $statement->execute(array(
                'game_name' => $game->getName(),
                'dev_id' => $game->getDeveloper()->getId(),
                'parent' => $game->getParentGameId(),
                'image_ref' => $game->getImageRef()
            ));
            return $row_count;
        }

        public static function updateGame($game){
            $conn = getConnection();
            $sql = '
                UPDATE Game SET 
                    game_name = :game_name,
                    dev_id = :dev_id,
                    parent_game_id = :parent_game_id,
                    game_image_ref = :game_image_ref
                WHERE 
                    game_id = :game_id
                    ;';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'game_id' => $game->getId(),
                'game_name' => $game->getName(),
                'dev_id' => $game->getDeveloper()->getId(),
                'game_image_ref' => $game->getImageRef(),
                'parent_game_id' => $game->getParentGameId()
            ));
            return $row_count;
        }
        public static function deleteGameById($id){
            $conn = getConnection();
            $sql = "DELETE FROM Game WHERE game_id=:id";
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'id' => $id
            ));
            return $row_count;
        }
        public static function findGameByName($game_name){
            $conn = getConnection();
            $sql = "SELECT * FROM Game WHERE game_name LIKE :game_name";
            $statement = $conn->prepare($sql);
            $statement->execute(array('game_name' => "%{$game_name}%"));
            $raw_game_list = $statement->fetchAll();
            if ($raw_game_list === false) return NULL;
            return GameSQL::rawGameListToGame($raw_game_list);
        }
        public static function findGameByDeveloper($developer) {
            $conn = getConnection();
            $sql = "SELECT * FROM Game WHERE dev_id=:dev_id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('dev_id' => $developer->getId()));
            $raw_game_list = $statement->fetchAll();
            if ($raw_game_list === false) return NULL;
            return GameSQL::rawGameListToGame($raw_game_list);
        }
        public static function findGameById($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Game WHERE game_id=:id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_game = $statement->fetch();
            if($raw_game === false) return NULL;
            return GameSQL::rawGameToGame($raw_game);
        }
        public static function findDLCByGameId($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Game WHERE parent_game_id=:id;";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_game_list = $statement->fetchAll();
            if ($raw_game_list === false) return NULL;
            return GameSQL::rawGameListToGame($raw_game_list);
        }
        public static function findAll(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Game;");
            $stmt->execute();
            $raw_game_list = $stmt->fetchAll();
            if ($raw_game_list === false) return NULL;
            return GameSQL::rawGameListToGame($raw_game_list);
        }
        public static function findAllWithOutDLC(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Game WHERE parent_game_id IS NULL");
            $stmt->execute();
            $raw_game_list = $stmt->fetchAll();
            if ($raw_game_list === false) return NULL;
            return GameSQL::rawGameListToGame($raw_game_list);
        }
    }
?>

