<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/price.php';
    require_once ROOT . '/models/promotion.php';
    require_once ROOT . '/models/game.php';
    class PriceSQL {

        private static function rawPriceToPrice($raw_price){
            return Price::loadPrice($raw_price["price_id"], $raw_price["promotion_id"], $raw_price["game_id"], $raw_price["price"]);
        }
        private static function rawPriceListToPrice($raw_price_list){
            $price_list = array();
            foreach($raw_price_list as $raw_price){
                $price_list[] = PriceSQL::rawPriceToPrice($raw_price);
            }
            return $price_list;
        }
        public static function createPrice($price){
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Price (promotion_id, game_id, price) VALUES (:promotion_id, :game_id, :price)");
            $row_count = $statement->execute(array(
                'promotion_id' => $price->getPromotionId(),
                'game_id' => $price->getGameId(),
                'price' => $price->getPrice()
            ));
            return $row_count;
        }

        public static function updatePrice($price){
            $conn = getConnection();
            $sql = '
                UPDATE Price SET 
                    promotion_id=:promotion_id, 
                    game_id=:game_id,
                    price=:price
                WHERE 
                    price_id=:id
                    ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'promotion_id' => $price->getPromotionId(),
                'game_id' => $price->getGameId(),
                'price' => $price->getPrice(),
                'id' => $price->getId()
            ));
            return $row_count;
        }
        public static function deletePriceById($id){
            $conn = getConnection();
            $sql = "DELETE FROM Price WHERE price_id=:id";
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'id' => $id
            ));
            return $row_count;
        }
        public static function findPriceById($id){
            $conn = getConnection();
            $sql = "SELECT * FROM Price WHERE price_id=:id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_price = $statement->fetch();
            if(!$raw_price) return NULL;
            return PriceSQL::rawPriceToPrice($raw_price);
        }

        public static function findPriceByGameId($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Price WHERE game_id=:id ORDER BY price_id DESC";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_price_list = $statement->fetchAll();
            return PriceSQL::rawPriceListToPrice($raw_price_list);
        }
        public static function findByPriceRange($from, $to) {
            $conn = getConnection();
            $sql = "SELECT * FROM Price WHERE price BETWEEN :from AND :to";
            $statement = $conn->prepare($sql);
            $statement->execute(array('from' => $from, 'to' => $to));
            $raw_price_list = $statement->fetchAll();
            return PriceSQL::rawPriceListToPrice($raw_price_list);
        }
        public static function findPriceByPromotionId($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Price WHERE promotion_id = :id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_price_list = $statement->fetchAll();
            return PriceSQL::rawPriceListToPrice($raw_price_list);
            
        }
        public static function findAll(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Price");
            $stmt->execute();
            $raw_price_list = $stmt->fetchAll();
            return PriceSQL::rawPriceListToPrice($raw_price_list);
        }
    }
?>

