<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/offer.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/price.php';
    require_once ROOT . '/models/customer_manager.php';
    class OfferSQL {
        private static function rawOfferToOffer($raw_offer){
            return Offer::loadOffer($raw_offer["offer_id"], $raw_offer["cust_id"], $raw_offer["price_id"], $raw_offer["offer_time"]);
        }
        private static function rawOfferListToOffer($raw_offer_list){
            $offer_list = array();
            foreach($raw_offer_list as $raw_offer){
                $offer_list[] = OfferSQL::rawOfferToOffer($raw_offer);
            }
            return $offer_list;
        }
        public static function createOffer($offer){
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Offer (cust_id, price_id, offer_time) VALUES (:cust_id, :price_id, now())");
            $row_count = $statement->execute(array(
                'cust_id' => $offer->getCustomer()->getId(),
                'price_id' => $offer->getPrice()->getId()
            ));
            return $row_count;
        }

        public static function updateOffer($offer){
            $conn = getConnection();
            $sql = '
                UPDATE Offer SET
                    cust_id = :cust_id,
                    price_id = :price_id,
                    offer_time = :offer_time
                WHERE
                    offer_id = :offer_id
                ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'offer_id' => $offer->getId(),
                'cust_id' => $offer->getCustomer()->getId(),
                'price_id' => $offer->getPrice()->getId(),
                'offer_time' => $offer->getTime()
            ));
            return $row_count;
        }

        public static function findOfferById($id){
            $conn = getConnection();
            $sql = "SELECT * FROM Offer WHERE offer_id=:id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_offer = $statement->fetch();
            if(!$raw_offer) return NULL;
            return OfferSQL::rawOfferToOffer($raw_offer);
        }
        public static function findOfferByCustId($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Offer WHERE cust_id=:id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_offer = $statement->fetchAll();
            if($raw_offer === false) return NULL;
            return OfferSQL::rawOfferListToOffer($raw_offer);
        }

        public static function findOfferByCustIdAndGameId($cust_id,$game_id)  {
            $conn = getConnection();
            $sql = "SELECT * FROM Offer, Price WHERE Offer.cust_id=:cust_id
                        AND Offer.price_id=Price.price_id
                        AND Price.game_id=:game_id
                        ORDER BY Offer.offer_id DESC
                        LIMIT 1";
            $statement = $conn->prepare($sql);
            $statement->execute(array(
                                'cust_id' => $cust_id,
                                'game_id' => $game_id
                                ));
            $raw_offer = $statement->fetchAll();
            print_r($raw_offer);
            if($raw_offer === false) return NULL;
            return OfferSQL::rawOfferListToOffer($raw_offer);
        }

        public static function findOfferByCustUsername($username) {
            $customer = CustomerManager::findCustomerByUsername($username);
            if ($customer === NULL) return NULL;
            return OfferSQL::findOfferByCustId($customer->getId());
        }

        public static function findOfferByPrice($price){
            $conn = getConnection();
            $sql = '
                SELECT * FROM Offer WHERE price_id = :price_id
                ';
            $statement = $conn->prepare($sql);
            $statement->execute(array(
                'price_id' => $price->getId()
            ));
            $raw_offer = $statement->fetchAll();
            if($raw_offer === false) return NULL;
            return OfferSQL::rawOfferListToOffer($raw_offer);
        }

        public static function findAll(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Offer");
            $stmt->execute();
            $raw_offer = $statement->fetchAll();
            if($raw_offer !== false)
                return OfferSQL::rawOfferListToOffer($raw_offer);
            else
                return NULL;
        }
    }
?>

