<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/promotion.php';
    class PromotionSQL {
        private static function rawPromotionListToPromotion($raw_promotion_list){
            $promotion_list =  array();
            foreach($raw_promotion_list as $raw_promotion){
                $promotion_list[] = PromotionSQL::rawPromotionToPromotion($raw_promotion);
            }
            return $promotion_list;
        }

        private static function rawPromotionToPromotion($raw_promotion){
            return Promotion::loadPromotion($raw_promotion["promotion_id"], $raw_promotion["promotion_name"], $raw_promotion["start_date"], $raw_promotion["end_date"]);
        }
        public static function createPromotion($promotion){
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Promotion (promotion_name, start_date, end_date) VALUES (:pro_name, :start, :end )");
            $row_count = $statement->execute(array(
                'pro_name' => $promotion->getName(),
                'start' => $promotion->getStartDate(),
                'end' => $promotion->getEndDate()
            ));
            return $row_count;
        }
        public static function deletePromotionById($id) {
            $conn = getConnection();
            $statement = $conn->prepare("DELETE FROM Promotion WHERE promotion_id=:id");
            $row_count = $statement->execute(array('id' => $id));
            return $row_count;
        }
        public static function updatePromotion($promotion){
            $conn = getConnection();
            $sql = '
                UPDATE Promotion SET
                    promotion_name = :promotion_name,
                    start_date = :start_date,
                    end_date = :end_date
                WHERE
                    promotion_id = :promotion_id
                    ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'promotion_id' => $promotion->getId(),
                'promotion_name' => $promotion->getName(),
                'start_date' => $promotion->getStartDate(),
                'end_date' => $promotion->getEndDate()
            ));
            return $row_count;
        }
        public static function findPromotionByName($name) {
            $conn = getConnection();
            $sql = "SELECT * FROM Promotion WHERE promotion_name LIKE :name";
            $statement = $conn->prepare($sql);
            $statement->execute(array('name' => "%".$name."%"));
            $raw_promotion_list = $statement->fetchAll();
            if ($raw_promotion_list === false) return NULL;
            else return PromotionSQL::rawPromotionListToPromotion($raw_promotion_list);
            
        }
        public static function findPromotionById($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Promotion WHERE promotion_id = :pro_id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('pro_id' => $id));
            $raw_promotion = $statement->fetch();
            if ($raw_promotion === false) return NULL;
            else return PromotionSQL::rawPromotionToPromotion($raw_promotion);
        }
        //public static function findPromotionByGameId($id) {
        //    $conn = getConnection();
        //    $sql = "SELECT * FROM Promotion WHERE game_id=:game_id";
        //    $statement = $conn->prepare($sql);
        //    $statement->execute(array('game_id' => $id));
        //    $raw_promotion_list = $statement->fetchAll();
        //    if ($raw_promotion_list === false) return NULL;
        //    return PromotionSQL::rawPromotionListToPromotion($raw_promotion_list);
        //}
        public static function findAll(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Promotion");
            $stmt->execute();
            $raw_promotion_list = $statement->fetchAll();
            if ($raw_promotion_list === false) return NULL;
            return PromotionSQL::rawPromotionListToPromotion($raw_promotion_list);
        }
        public static function findActivePromotion() {
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Promotion WHERE :this_time BETWEEN start_date AND end_date;");
            $stmt->execute(array('this_time' => date("Y-m-d H:m:s", time())));
            $raw_promotion_list = $stmt->fetchAll();
            if ($raw_promotion_list === false) return NULL;
            return PromotionSQL::rawPromotionListToPromotion($raw_promotion_list);
        }
    }
?>

