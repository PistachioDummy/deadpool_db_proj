<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/offer.php';
    require_once ROOT . '/models/refund.php';
    class RefundSQL {

        private static function rawRefundToRefund($raw_refund){
            return Refund::loadRefund($raw_refund["refund_id"], $raw_refund["offerId"], $raw_refund["refund_time"]);
        }
        private static function rawRefundListToRefund($raw_refund_list){
            $refund_list = array();
            foreach($raw_refund_list as $raw_refund){
                $refund[] = RefundSQL::rawRefundToRefund($raw_refund);
            }
            return $refund_list;
        }
        public static function createRefund($refund){
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Refund (offer_id, refund_time) VALUES (:offer_id, now())");
            $row_count = $statement->execute(array(
                'offer_id' => $refund->getOffer()->getId()
            ));
            return $row_count;
        }

        public static function updateRefund($refund){
            $conn = getConnection();
            $sql = '
                UPDATE Refund SET
                    offer_id = :offer_id,
                    refund_time = :refund_time
                WHERE
                    refund_id = :refund_id,
                ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'refund_id' => $refund->getId(),
                'offer_id' => $refund->getOffer()->getId(),
                'refund_time' => $refund->getTime()
            ));
            return $row_count;
        }

        public static function findRefundById($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Refund WHERE refund_id=:id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_refund = $statement->fetch();
            if ($raw_refund === false) return NULL;
            return RefundSQL::rawRefundToRefund($raw_refund);
        }
        public static function findAll(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Refund");
            $stmt->execute();
            $raw_refund_list = $statement->fetchAll();
            if ($raw_refund_list === false) return NULL;
            return RefundSQL::rawRefundListToRefund($raw_refund_list);
        }

        public static function findRefundByOfferId($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM Refund WHERE offer_id=:id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_refund = $statement->fetch();
            if ($raw_refund === false) return NULL;
            return RefundSQL::rawRefundToRefund($raw_refund);
        }
        public static function findRefundByOffer($offer) {
            return RefundSQL::findRefundByOfferId($offer->getId());
        }
        public static function findRefundByTime($from, $to) {
            $conn = getConnection();
            $sql = "SELECT * FROM Refund WHERE refund_time BETWEEN :from AND :to";
            $statement = $conn->prepare($sql);
            $statement->execute(array('from' => $from, 'to' => $to));
            $raw_refund_list = $statement->fetchAll();
            if ($raw_refund_list === false) return NULL;
            return RefundSQL::rawRefundListToRefund($raw_refund_list);
        }
    }
?>

