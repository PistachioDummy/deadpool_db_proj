<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/topup.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/customer_manager.php';
    class TopupSQL {
        private static function rawTopupToTopup($raw_topup){
            return Topup::loadTopup($raw_topup["topup_id"], $raw_topup["cust_id"], $raw_topup["topup_money"], $raw_topup["topup_time"]);
        }

        private static function rawTopupListToTopup($raw_topup_list){
            $topup_list = array();
            foreach($raw_topup_list as $raw_topup){
                $topup_list[] = TopupSQL::rawTopupToTopup($raw_topup);
            }
            return $topup_list;
        }
        public static function isRowExistsByPk($topup_id) {
            $conn = getConnection();
            $sql = "
                SELECT * FROM Topup WHERE topup_id=:topup_id;
                ";
            $statement = $conn->prepare($sql);
            $statement->execute(array(
                'topup_id' => $topup_id
            ));
            if ($statement->fetch()) return True;
            return False;
        }

        public static function createTopup($topup) {
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Topup (cust_id, topup_money, topup_time) VALUES (:cust_id, :topup_money, :time)");
            $row_count = $statement->execute(array(
                'cust_id' => $topup->getCustomer()->getID(),
                'topup_money' => $topup->getMoney(),
                'time' => $topup->getTime()
            ));
            return $row_count;
        }

        public static function findTopupByUsername($username) {
            $customer = CustomerManager::findCustomerByUsername($username);
            return TopupSQL::findTopupByCustomerObject($customer);
        }

        public static function updateTopup($topup) {
            $conn = getConnection();
            $sql = '
                UPDATE Topup SET
                    cust_id = :cust_id,
                    topup_money = :topup_money,
                    topup_time = :topup_time
                WHERE
                    topup_id = :topup_id
                ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'topup_id' => $topup->getId(),
                'cust_id' => $topup->getCustomer()->getID(),
                'topup_money' => $topup->getMoney(),
                'topup_time' => $topup->getTime()
            ));
            return $row_count;
        }

        public static function findTopupByUserId($user_id) {
            $conn = getConnection();
            $statement = $conn->prepare("SELECT * FROM Topup WHERE cust_id=:cust_id");
            $statement->execute(array(
                'cust_id' => $customer->getId()
            ));
            $raw_topup_list = $stmt->fetchAll();
            if ($raw_topup_list === false) return NULL;
            return TopupSQL::rawTopupListToTopup($raw_topup_list);
        }
        
        // find by customer object
        public static function findTopupByCustomerObject($customer) {
            return TopupSQL::findTopupByUserId($customer->getId());
        }

        public static function findTopupById($id) {
            $conn = getConnection();
            $statement = $conn->prepare("SELECT * FROM Topup WHERE topup_id=:topup_id");
            $statement->execute(array(
                'topup_id' => $id
            ));
            $raw_topup = $stmt->fetch();
            if ($raw_topup === false) return NULL;
            return TopupSQL::rawTopupToTopup($raw_topup);
        }
        
        public static function findAll() {
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Topup");
            $stmt->execute();
            $raw_topup_list = $stmt->fetchAll();
            if ($raw_topup_list === false) return NULL;
            return TopupSQL::rawTopupListToTopup($raw_topup_list);
        }
    }
?>
