<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/topup.php';
    require_once ROOT . '/models/developer.php';
    class DeveloperSQL {
        public static function isRowExistsByPk($dev_id) {
            $conn = getConnection();
            $sql = "
                SELECT * FROM Developer WHERE dev_id=:dev_id;
                ";
            $statement = $conn->prepare($sql);
            $statement->execute(array(
                'dev_id' => $dev_id
            ));
            if ($statement->fetch()) return True;
            return False;
        }
        private static function rawDeveloperToDeveloper($raw_dev){
            return Developer::loadDeveloper($raw_dev["dev_id"], $raw_dev["dev_name"], $raw_dev["dev_phonenumber"], $raw_dev["dev_city"]);
        }

        private static function rawDeveloperListToDeveloper($raw_dev_list){
            $dev_list = array();
            foreach($raw_dev_list as $raw_dev){
                $dev_list[] = DeveloperSQL::rawDeveloperToDeveloper($raw_dev);
            }
            return $dev_list;
        }

        public static function updateDeveloper($dev){
            $conn = getConnection();
            $sql = '
                UPDATE Developer SET
                    dev_name = :dev_name,
                    dev_phonenumber = :dev_phonenumber,
                    dev_city = :dev_city
                WHERE 
                    dev_id = :dev_id
                ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'dev_id' => $dev->getId(),
                'dev_name' => $dev->getName(),
                'dev_phonenumber' => $dev->getPhonenumber(),
                'dev_city' => $dev->getCity()
            ));
            return $row_count;
        }

        public static function createDeveloper($dev) {
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Developer (dev_name, dev_phonenumber, dev_city) VALUES (:dev_name, :dev_phonenumber, :dev_city);
            ");
            $row_count = $statement->execute(array(
                'dev_name' => $dev->getName(),
                'dev_phonenumber' => $dev->getPhonenumber(),
                'dev_city' => $dev->getCity()
            ));
            return $row_count;
        }

        public static function deleteDeveloperById($dev_id) {
            $conn = getConnection();
            $statement = $conn->prepare("DELETE FROM Developer WHERE dev_id=:id");
            $row_count = $statement->execute(array(
                'id' => $dev_id
            ));
            return $row_count;
        }

        public static function findDeveloperById($dev_id) {
            $conn = getConnection();
            $statement = $conn->prepare("SELECT * FROM Developer WHERE dev_id=:dev_id");
            $statement->execute(array('dev_id' => $dev_id));
            $raw_dev = $statement->fetch();
            if ($raw_dev === false) return NULL;
            return DeveloperSQL::rawDeveloperToDeveloper($raw_dev);
        }

        public static function findDeveloperByPhonenumber($dev_phone) {
            $conn = getConnection();
            $statement = $conn->prepare("SELECT * FROM Developer WHERE dev_phonenumber=:dev_phone");
            $statement->execute(array('dev_phone' => $dev_phone));
            $raw_dev = $statement->fetch();
            if ($raw_dev === false) return NULL;
            return DeveloperSQL::rawDeveloperToDeveloper($raw_dev);
        }

        public static function findDeveloperByCity($dev_city) {
            $conn = getConnection();
            $statement = $conn->prepare("SELECT * FROM Developer WHERE dev_city=:dev_city");
            $statement->execute(array('dev_city' => $dev_city));
            $raw_dev_list = $statement->fetchAll();
            if ($raw_dev_list === false) return NULL;
            return DeveloperSQL::rawDeveloperListToDeveloper($raw_dev_list);
        }

        public static function findDeveloperByName($dev_name) {
            $conn = getConnection();
            $statement = $conn->prepare("SELECT * FROM Developer WHERE dev_name=:dev_name");
            $statement->execute(array('dev_name' => $dev_name));
            $raw_dev = $statement->fetch();
            if ($raw_dev === false) return NULL;
            return DeveloperSQL::rawDeveloperToDeveloper($raw_dev);
        }

        public static function findAll() {
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Developer");
            $stmt->execute();
            $raw_dev_list = $stmt->fetchAll();
            if ($raw_dev_list === false) return NULL;
            return DeveloperSQL::rawDeveloperListToDeveloper($raw_dev_list);
        }
    }
?>
