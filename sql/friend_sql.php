<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/customer.php';
    class FriendSQL {
        public static function isRowExistsByPk($cust_id, $friend_id){
            $conn = getConnection();
            $sql = "
             SELECT * FROM Friend WHERE cust_id=:cust_id AND friend_id=:friend_id;
                ";
            $statement = $conn->prepare($sql);
            $statement->execute(array(
                'cust_id' => $cust_id,
                'friend_id' => $friend_id
            ));
            if($statement->fetch()) return True;
            return False;
        }
        public static function createFriend($customer_a, $customer_b){
            $conn = getConnection();
            $sql = "
                INSERT INTO Friend (cust_id, friend_id, friend_status) 
                VALUES             (:id, :friend_id, 0)
            ";
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'id' => $customer_a->getId(),
                'friend_id' => $customer_b->getId()
            ));
            if(!$row_count) return False;
            return True;
        }
        public static function acceptFriendStatus($customer_a, $customer_b){
            $conn = getConnection();
            $sql = "
                UPDATE Friend SET 
                    friend_status=1
                WHERE 
                    cust_id = :id
                    AND friend_id = :friend_id
            ";
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'id' => $customer_a->getId(),
                'friend_id' => $customer_b->getId()
            ));
            if(!$row_count) return False;
            return True;
        }
        public static function deleteFriendStatus($customer_a, $customer_b){
            $conn = getConnection();
            $sql = "
                DELETE FROM Friend 
                WHERE (cust_id = :id AND friend_id = :friend_id)
                OR (cust_id = :friend_id AND friend_id = :id);
            ";
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'id' => $customer_a->getId(),
                'friend_id' => $customer_b->getId()
            ));
            if(!$row_count) return False;
            return True;
        }
        public static function findAll(){
            $conn = getConnection();
            $statement = $conn->prepare("SELECT * FROM Friend");
            $statement->execute();
            return $statement->fetchAll();
        }
        public static function findAcceptedFriendsIdByCustId($cust_id){
            $conn = getConnection();
            $statement = $conn->prepare("SELECT friend_id FROM Friend WHERE cust_id=:cust_id AND friend_status=1");
            $row_count = $statement->execute(array(
                'cust_id' => $cust_id
            ));
            $raw_friend_id_list = $statement->fetchAll();
            $friend_id_list = array();
            foreach($raw_friend_id_list as $raw_friend_id){
                $friend_id_list[] = $raw_friend_id["friend_id"];
            }
            $statement = $conn->prepare("SELECT cust_id FROM Friend WHERE friend_id=:cust_id AND friend_status=1");
            $row_count = $statement->execute(array(
                'cust_id' => $cust_id
            ));
            $raw_friend_id_list = $statement->fetchAll();
            foreach($raw_friend_id_list as $raw_friend_id){
                $friend_id_list[] = $raw_friend_id["cust_id"];
            }
            return $friend_id_list;
        }
        public static function findWaitingForAcceptFriendIdByCustId($cust_id) {
            $conn = getConnection();
            $statement = $conn->prepare("SELECT friend_id FROM Friend WHERE cust_id=:cust_id and friend_status=0");
            $row_count = $statement->execute(array(
                'cust_id' => $cust_id
            ));
            $raw_friend_id_list = $statement->fetchAll();
            $friend_id_list = array();
            foreach($raw_friend_id_list as $raw_friend_id){
                $friend_id_list[] = $raw_friend_id["friend_id"];
            }
            return $friend_id_list;
        }
        public static function findPendingFriendIdByCustId($id){
            $conn = getConnection();
            $statement = $conn->prepare("SELECT cust_id FROM Friend WHERE friend_id=:id and friend_status=0");
            $row_count = $statement->execute(array(
                'id' => $id
            ));
            $raw_friend_id_list = $statement->fetchAll();
            $friend_id_list = array();
            foreach($raw_friend_id_list as $raw_friend_id){
                $friend_id_list[] = $raw_friend_id["cust_id"];
            }
            return $friend_id_list;
        }

        public static function findSuggestedFriend($cust_id){
            $conn = getConnection();
            $sql = '
                SELECT c1.cust_id as FirstUser,
                    c2.cust_id as SecondUser,
                    COUNT(c1.friend_id) as MutualFriend
                    FROM Friend c1
                    INNER JOIN Friend c2
                    ON c1.friend_id = c2.friend_id AND c1.cust_id != c2.cust_id AND c1.cust_id != c2.friend_id AND c2.cust_id != c1.friend_id
                    WHERE c1.cust_id = :cust_id
                    GROUP BY c1.cust_id, c2.cust_id
                    ORDER BY COUNT(c1.friend_id) DESC
                    LIMIT 10
                    ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'cust_id' => $cust_id
            ));
            if(!$row_count) return False;
            return True;
        }
    }
?>
