<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/customer.php';
    class CustomerSQL {
        public static function createCustomer($customer){
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO Customer (cust_username, cust_password, cust_profilename, cust_money, cust_email, cust_image_ref) VALUES (:username, :password, :profilename, :money, :email, :image_ref)");
            $row_count = $statement->execute(array(
                'username' => $customer->getUsername(),
                'password' => $customer->getPassword(),
                'profilename' => $customer->getProfileName(),
                'money' => $customer->getMoney(),
                'email' => $customer->getEmail(),
                'image_ref' => $customer->getImageRef()
            ));
            return $row_count;
        }
        public static function updateCustomer($customer){
            $conn = getConnection();
            $sql = '
                UPDATE Customer SET 
                    cust_profilename=:profilename, 
                    cust_money=:money, 
                    cust_email=:email,
                    cust_password=:password,
                    cust_image_ref=:image_ref
                WHERE 
                    cust_id=:id
                    ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'profilename' => $customer->getProfileName(),
                'money' => $customer->getMoney(),
                'password' => $customer->getPassword(),
                'email' => $customer->getEmail(),
                'id' => $customer->getId(),
                'image_ref' => $customer->getImageRef()
            ));
            return $row_count;
        }
        public static function deleteCustomerById($id){
            $conn = getConnection();
            $sql = "DELETE FROM Customer WHERE cust_id=:id";
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'id' => $id
            ));
            return $row_count;
        }
        public static function findCustomerByUsername($username){
            $conn = getConnection();
            $sql = "SELECT * FROM Customer WHERE cust_username = :username";
            $statement = $conn->prepare($sql);
            $statement->execute(array('username' => $username));
            $raw_customer = $statement->fetch();
            if(!$raw_customer) return NULL;
            return CustomerSQL::rawCustomerToCustomer($raw_customer);
        }
        private static function rawCustomerToCustomer($raw_customer) {
            return Customer::loadCustomer($raw_customer["cust_id"],
                $raw_customer["cust_username"],
                $raw_customer["cust_password"],
                $raw_customer["cust_profilename"],
                $raw_customer["cust_money"],
                $raw_customer["cust_email"],
                $raw_customer["cust_image_ref"]);
        }
        private static function rawCustomerListToCustomer($raw_cust_list){
            $cust_list = array();
            foreach($raw_cust_list as $raw_cust){
                $cust_list[] = CustomerSQL::rawCustomerToCustomer($raw_cust);
            }
            return $cust_list;
        }
        public static function findCustomerById($id){
            $conn = getConnection();
            $sql = "SELECT * FROM Customer WHERE cust_id=:id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('id' => $id));
            $raw_customer = $statement->fetch();
            if(!$raw_customer) return NULL;
            return CustomerSQL::rawCustomerToCustomer($raw_customer); 
        }
        public static function findAll(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM Customer");
            $stmt->execute();
            $raw_cust_list = $stmt->fetchAll();
            if (!$raw_cust_list) {
                return NULL;
            }
            else {
                return CustomerSQL::rawCustomerListToCustomer($raw_cust_list);
            }
        }

        public static function findSuggestedFriendById($cust_id){
            $conn = getConnection();
            $sql = '
                SELECT c1.cust_id as FirstUser,
                    c2.cust_id as SecondUser,
                    COUNT(c1.friend_id) as MutualFriend
                    FROM Friend c1
                    INNER JOIN Friend c2
                    ON c1.friend_id = c2.friend_id AND c1.cust_id != c2.cust_id AND c1.cust_id != c2.friend_id AND c2.cust_id != c1.friend_id
                    WHERE c1.cust_id = :cust_id
                    GROUP BY c1.cust_id, c2.cust_id
                    ORDER BY COUNT(c1.friend_id) DESC
                    LIMIT 10
                    ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'cust_id' => $cust_id
            ));
            $raw_cust_list = $statement->fetchAll();
            return $cust_list;
        }
    }
?>

