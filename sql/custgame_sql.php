<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/custgame.php';
    require_once ROOT . '/models/game.php';
    class CustGameSQL {

        private static function rawCustGameToCustGame($raw_custgame){
            return CustGame::loadCustGame($raw_custgame["cust_id"], $raw_custgame["game_id"], $raw_custgame["play_time"]);
        }
        private static function rawCustGameListToCustGame($raw_custgame_list){
            $custgame_list = array();
            foreach($raw_custgame_list as $raw_custgame){
                $custgame_list[] = CustGameSQL::rawCustGameToCustGame($raw_custgame);
            }
            return $custgame_list;
        }
        public static function updateCustGame($custgame) {
            $conn = getConnection();
            $sql = '
                UPDATE CustGame SET 
                    play_time=:time
                WHERE 
                    cust_id=:id AND
                    game_id=:game_id
                    ';
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'id' => $custgame->getId(),
                'game_id' => $custgame->getGame()->getId(),
                'time' => $custgame->getPlayTime()
            ));
            return $row_count;
        }
        public static function createCustGame($cust_id, $game_id){
            $conn = getConnection();
            $statement = $conn->prepare("INSERT INTO CustGame (cust_id, game_id, play_time) VALUES (:cust_id, :game_id, 0)");
            $row_count = $statement->execute(array(
                'cust_id' => $cust_id,
                'game_id' => $game_id
            ));
            return $row_count;
        }

        public static function deleteCustGame($cust_id, $game_id){
            $conn = getConnection();
            $sql = "DELETE FROM CustGame WHERE cust_id=:cust_id AND game_id=:game_id";
            $statement = $conn->prepare($sql);
            $row_count = $statement->execute(array(
                'cust_id' => $cust_id,
                'game_id' => $game_id
            ));
            return $row_count;
        }
        public static function findCustGame($cust_id, $game_id) {
            $conn = getConnection();
            $sql = "SELECT * FROM CustGame WHERE cust_id=:cust_id AND game_id=:game_id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('cust_id' => $cust_id, 'game_id' => $game_id));
            $raw_custgame = $statement->fetch();
            return CustGameSQL::rawCustGameToCustGame($raw_custgame);
        }
        public static function findCustGameByCustomerId($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM CustGame WHERE cust_id=:cust_id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('cust_id' => $id));
            $raw_custgame_list = $statement->fetchAll();
            return CustGameSQL::rawCustGameListToCustGame($raw_custgame_list);
        }
        public static function findCustGameByGameId($id) {
            $conn = getConnection();
            $sql = "SELECT * FROM CustGame WHERE game_id=:game_id";
            $statement = $conn->prepare($sql);
            $statement->execute(array('game_id' => $id));
            $raw_custgame_list = $statement->fetchAll();
            return CustGameSQL::rawCustGameListToCustGame($raw_custgame_list);
        }
        public static function findAll(){
            $conn = getConnection();
            $stmt = $conn->prepare("SELECT * FROM CustGame");
            $stmt->execute();
            $raw_custgame_list = $stmt->fetchAll();
            return CustGameSQL::rawCustGameListToCustGame($raw_custgame_list);
        }
    }
?>

