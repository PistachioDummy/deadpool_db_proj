<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/setup/customer_table.php';
    require_once ROOT . '/setup/friend_table.php';
    require_once ROOT . '/models/customer.php';
    require_once ROOT . '/models/customer_manager.php';

function testing($va){
    if($va){
        echo "True\n";
    }else{
        echo "False\n";
    }
}
/* Clean data */
deleteFriendTable();
deleteCustomerTable();
createCustomerTable();
createFriendTable();

$customers = array(
    Customer::newCustomer("AAA", "AAA", "AAA", 0, "test", NULL),
    Customer::newCustomer("BBB", "BBB", "AAA", 0, "BBB", NULL),
    Customer::newCustomer("CCC", "CCC", "AAA", 0, "CCC", NULL),
    Customer::newCustomer("DDD", "DDD", "AAA", 0, "DDD", NULL),
    Customer::newCustomer("EEE", "EEE", "AAA", 0, "EEE", NULL),
);
//print_r(Customer::newCustomer("EEE", "EEE", "AAA", 0, "test"));
//print "\n";

foreach($customers as $customer){
    //echo $customer.toArray();
    CustomerManager::createCustomer($customer);
}

assert(CustomerManager::findCustomerByUserName("AAA")->getUsername() === 'AAA') ;
assert(CustomerManager::findCustomerById("1")->getUsername() === 'AAA');

$customer = CustomerManager::findCustomerById("1");
$customer->setEmail("ZZZ");
$customer->save();
$customer = CustomerManager::findCustomerById("1");

assert(CustomerManager::findCustomerById("1")->getEmail() === 'ZZZ');
assert($customer->checkPassword("AAA") == true);
assert($customer->toJSON() === '{"id":"1","username":"AAA","name":"AAA","money":"0","email":"ZZZ"}');
CustomerManager::deleteCustomer($customer);
assert(CustomerManager::findCustomerById("1") === NULL);

deleteFriendTable();
deleteCustomerTable();
?>
