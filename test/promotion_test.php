<?php
defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
require_once ROOT.'/models/promotion.php';
require_once ROOT.'/models/promotion_manager.php';
require_once ROOT.'/setup/promotion_table.php';

deletePromotionTable();
createPromotionTable();

$promos = array(
    Promotion::newPromotion("Holiday Season", '2015-12-25 00:00:00', '2015-12-31 00:00:00'),
    Promotion::newPromotion("Summer Sale", '2016-04-12 00:00:00', '2016-04-15 00:00:00'),
    Promotion::newPromotion("Spring Sale", '2016-09-08 00:00:00', '2016-09-31 00:00:00'),
    Promotion::newPromotion("Hot Sale", '2016-05-01 00:00:00', '2016-05-30 00:00:00')
);
foreach ($promos as $p) {
    PromotionManager::createPromotion($p);
}

$promo1 = PromotionManager::findActivePromotion();
assert($promo1[0]->getName() == "Hot Sale");
assert(PromotionManager::findPromotionById("1")->getName() == "Holiday Season");
assert(PromotionManager::findPromotionById("2")->getStartDate() == '2016-04-12 00:00:00');
PromotionManager::deletePromotionById("2");
assert(PromotionManager::findPromotionById("2") === NULL);

deletePromotionTable();
?>
