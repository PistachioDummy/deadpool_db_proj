<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
require_once ROOT.'/models/offer.php';
require_once ROOT.'/models/offer_manager.php';
require_once ROOT.'/models/customer.php';
require_once ROOT.'/models/customer_manager.php';
require_once ROOT.'/models/price.php';
require_once ROOT.'/models/price_manager.php';

require_once ROOT.'/setup/offer_table.php';
require_once ROOT.'/setup/customer_table.php';
require_once ROOT.'/setup/price_table.php';

createCustomerTable();
createPriceTable();
createOfferTable();
$cust = Customer::newCustomer("AAA", "AAA", "AAA", 0, "test", NULL);
CustomerManager::createCustomer($cust);
$price = Price::newPrice(NULL, "1", 55);
PriceManager::createPrice($price);
$offer = Offer::newOffer("1", "1", date("Y-m-d H:m:s" , time()));
OfferManager::createOffer($offer);
$offer1 = OfferManager::findOfferById("1");
echo($offer1->toJSON());
?>
