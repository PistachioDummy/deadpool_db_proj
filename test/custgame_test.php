<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
require_once ROOT.'/setup/game_table.php';
require_once ROOT.'/setup/custgame_table.php';
require_once ROOT.'/models/customer_manager.php';
require_once ROOT.'/setup/customer_table.php';
require_once ROOT.'/models/custgame_manager.php';
    require_once ROOT.'/setup/developer_table.php';
    require_once ROOT.'/models/game_manager.php';
    deleteCustGameTable();
    deleteGameTable();
    deleteDeveloperTable();
    deleteCustomerTable();
    createCustomerTable();
    createDeveloperTable();
    createGameTable();
    createCustGameTable();

    $devs = array(
        Developer::newDeveloper("AAA", "0929394444", "CA"),
        Developer::newDeveloper("Pistachio", "0182993999", "TH")
    );

    foreach ($devs as $dev) {
        DeveloperManager::createDeveloper($dev);
    }

    $dev1 = DeveloperManager::findDeveloperById("1");
    assert($dev1->getName() === "AAA");

    $games = array(
        Game::newGame("Deadpool", "1", NULL, NULL),
        Game::newGame("Deadpool2", $dev1->getId(), NULL, NULL),
        Game::newGame("DeadpoolDLC", $dev1->getId(), "2", NULL),
        Game::newGame("Hello", DeveloperManager::findDeveloperByName("Pistachio")->getId(), NULL, NULL)
    );
    foreach ($games as $g) {
        GameManager::createGame($g);
    }

    $game1 = GameManager::findGameById("1");
    $game2 = GameManager::findGameByName("Deadpool2")[0];
    //when delete game 2 and dlc of it would be deleted too
    $game3 = GameManager::findDLCByGameId($game2->getId());


$customers = array(
    Customer::newCustomer("AAA", "AAA", "AAA", 0, "AAA", NULL),
    Customer::newCustomer("BBB", "BBB", "AAA", 0, "BBB", NULL),
    Customer::newCustomer("CCC", "CCC", "AAA", 0, "CCC", NULL),
);

foreach($customers as $customer){
    //echo $customer.toArray();
    CustomerManager::createCustomer($customer);
}

$cust1 = CustomerManager::findCustomerByUsername("AAA");
$custgame1 = CustGame::newCustGame($cust1->getId(), $game1->getId(), 0);

CustGameManager::createCustGame($custgame1);
$custgame1 = CustGameManager::findCustGame($cust1->getId(), $game1->getId());
    
deleteCustGameTable();
deleteCustomerTable();
    deleteGameTable();
    deleteDeveloperTable();
?>
