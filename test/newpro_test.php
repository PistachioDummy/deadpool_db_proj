<?php
    
defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
require_once ROOT.'/models/promotion.php';
require_once ROOT.'/models/promotion_manager.php';
require_once ROOT.'/setup/promotion_table.php';


$pro1 = Promotion::newPromotion("Hot Sale", '2016-05-01 00:00:00', '2016-05-30 00:00:00');
PromotionManager::createPromotion($pro1);
echo (PromotionManager::findPromotionById("1")->toJSON());
?>
