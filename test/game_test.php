<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT.'/setup/game_table.php';
    require_once ROOT.'/setup/developer_table.php';
    require_once ROOT.'/models/game.php';
    require_once ROOT.'/models/game_manager.php';

    deleteGameTable();
    deleteDeveloperTable();
    createDeveloperTable();
    createGameTable();

    $devs = array(
        Developer::newDeveloper("AAA", "0929394444", "CA"),
        Developer::newDeveloper("Pistachio", "0182993999", "TH")
    );

    foreach ($devs as $dev) {
        DeveloperManager::createDeveloper($dev);
    }

    $dev1 = DeveloperManager::findDeveloperById("1");
    assert($dev1->getName() === "AAA");

    $games = array(
        Game::newGame("Deadpool", "1", NULL, NULL),
        Game::newGame("Deadpool2", $dev1->getId(), NULL, NULL),
        Game::newGame("DeadpoolDLC", $dev1->getId(), "2", NULL),
        Game::newGame("Hello", DeveloperManager::findDeveloperByName("Pistachio")->getId(), NULL, NULL)
    );
    foreach ($games as $g) {
        GameManager::createGame($g);
    }

    $game1 = GameManager::findGameById("1");
    assert($game1->getName() === "Deadpool");
    assert($game1->getDeveloperId() === "1");
    $game2 = GameManager::findGameByName("Deadpool2")[0];
    assert($game2->getName() === "Deadpool2");
    assert (GameManager::deleteGame($game2) !== false);
    //when delete game 2 and dlc of it would be deleted too
    $game3 = GameManager::findDLCByGameId($game2->getId());
    assert($game3 === array());

    deleteGameTable();
    deleteDeveloperTable();
?>
