<?php
    defined ("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/setup/developer_table.php';
    require_once ROOT . '/models/developer.php';
    require_once ROOT . '/models/developer_manager.php';

/* Clean data */
deleteDeveloperTable();
createDeveloperTable();

assert(DeveloperManager::findAll() === array());

$developers = array(
    Developer::newDeveloper("AAA", "0829399200", "Bangkok"),
    Developer::newDeveloper("BBB", "0894448959", "CA"),
    Developer::newDeveloper("CCC", "0489239400", "TX"),
    Developer::newDeveloper("DDD", "0293945900", "Tokyo"),
);

//print_r(Customer::newCustomer("EEE", "EEE", "AAA", 0, "test"));
//print "\n";

foreach($developers as $dev){
    //echo $customer.toArray();
    DeveloperManager::createDeveloper($dev);
}
assert(DeveloperManager::findDeveloperByName("AAA")->getName() === "AAA");
assert(DeveloperManager::findDeveloperByPhonenumber("0293945900")->getName() === "DDD");
$dev1 = DeveloperManager::findDeveloperById("1");
$dev1->setPhonenumber("0182993990");
$dev1->save();

assert(DeveloperManager::findDeveloperById("1")->getPhonenumber() === "0182993990");
assert($dev1->toJSON() === '{"id":"1","name":"AAA","phonenumber":"0182993990","city":"Bangkok"}');

DeveloperManager::deleteDeveloper($dev1);
$dev2 = DeveloperManager::findDeveloperById("1");
assert(DeveloperManager::findDeveloperById("1") === NULL);

deleteDeveloperTable();
?>
