<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);

    require_once ROOT . '/setup/customer_table.php';
    require_once ROOT . '/setup/friend_table.php';
    require_once ROOT . '/models/customer_manager.php';
deleteFriendTable();

deleteCustomerTable();
createCustomerTable();

createFriendTable();


$customers = array(
    Customer::newCustomer("AAA", "AAA", "AAA", 0, "AAA", NULL),
    Customer::newCustomer("BBB", "BBB", "AAA", 0, "BBB", NULL),
    Customer::newCustomer("CCC", "CCC", "AAA", 0, "CCC", NULL),
    Customer::newCustomer("DDD", "DDD", "AAA", 0, "DDD", NULL),
    Customer::newCustomer("EEE", "EEE", "AAA", 0, "EEE", NULL),
);
foreach($customers as $customer){
    CustomerManager::createCustomer($customer);
}

$customer_a = CustomerManager::findCustomerByUserName("AAA");
$customer_b = CustomerManager::findCustomerByUserName("BBB");

$customer_a->addFriend($customer_b);
$customer_b->acceptFriend($customer_a);
assert($customer_a->deleteFriend($customer_b) == 1);
assert($customer_a->deleteFriend($customer_b) === false);
$customer_a->addFriend($customer_b);

$customer_a = CustomerManager::findCustomerByUserName("CCC");
$customer_b = CustomerManager::findCustomerByUserName("DDD");
$customer_a->addFriend($customer_b);
$customer_b->rejectFriend($customer_a);

CustomerManager::findAllFriend();

deleteFriendTable();
deleteCustomerTable();
?>
