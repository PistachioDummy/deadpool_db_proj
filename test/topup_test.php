<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/setup/topup_table.php';
    require_once ROOT.'/models/topup.php';
    require_once ROOT.'/setup/customer_table.php';
    require_once ROOT.'/models/customer_manager.php';
    require_once ROOT.'/models/customer.php';
    require_once ROOT.'/models/topup_manager.php';
    deleteTopupTable();
    deleteCustomerTable();
    createCustomerTable();
    createTopupTable();
    
$customers = array(
    Customer::newCustomer("AAA", "AAA", "AAA", 0, "test", NULL),
    Customer::newCustomer("BBB", "BBB", "AAA", 0, "BBB", NULL),
    Customer::newCustomer("CCC", "CCC", "AAA", 0, "CCC", NULL),
    Customer::newCustomer("DDD", "DDD", "AAA", 0, "DDD", NULL),
    Customer::newCustomer("EEE", "EEE", "AAA", 0, "EEE", NULL)
);
    foreach ($customers as $cust) {
        CustomerManager::createCustomer($cust);
    }

    $cust1 = CustomerManager::findCustomerByUsername("AAA");
   
    $topup1 = Topup::newTopup($cust1->getId(), 50, NULL);
    TopupManager::createTopup($topup1);
    $cust1 = CustomerManager::findCustomerById($cust1->getId());
    //assert($cust1->getMoney() == 50); 

    deleteTopupTable();
    deleteCustomerTable();
?>
