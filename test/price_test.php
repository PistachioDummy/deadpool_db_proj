<?php
defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
require_once ROOT.'/models/promotion_manager.php';
require_once ROOT.'/models/game_manager.php';
require_once ROOT.'/models/developer_manager.php';
require_once ROOT.'/models/price_manager.php';
require_once ROOT.'/setup/promotion_table.php';
require_once ROOT.'/setup/game_table.php';
require_once ROOT.'/setup/price_table.php';
require_once ROOT.'/setup/developer_table.php';
/*
deletePriceTable();
deletePromotionTable();
deleteGameTable();
deleteDeveloperTable();
 */
createDeveloperTable();
createGameTable();
createPromotionTable();
createPriceTable();

$devs = array(
    Developer::newDeveloper("AAA", "0929394444", "CA"),
    Developer::newDeveloper("Pistachio", "0182993999", "TH")
);

foreach ($devs as $dev) {
    DeveloperManager::createDeveloper($dev);
}

$dev1 = DeveloperManager::findDeveloperById("1");
echo($dev1->toJSON());
assert($dev1->getName() === "AAA");

$games = array(
    Game::newGame("Deadpool", "1", NULL, NULL),
    Game::newGame("Deadpool2", $dev1->getId(), NULL, NULL),
    Game::newGame("DeadpoolDLC", $dev1->getId(), "2", NULL),
    Game::newGame("Hello", DeveloperManager::findDeveloperByName("Pistachio")->getId(), NULL, NULL)
);
foreach ($games as $g) {
    GameManager::createGame($g);
}

$promos = array(
    Promotion::newPromotion("Holiday Season", '2015-12-25 00:00:00', '2015-12-31 00:00:00'),
    Promotion::newPromotion("Summer Sale", '2016-04-12 00:00:00', '2016-04-15 00:00:00'),
    Promotion::newPromotion("Spring Sale", '2016-09-08 00:00:00', '2016-09-31 00:00:00'),
    Promotion::newPromotion("Hot Sale", '2016-05-01 00:00:00', '2016-05-30 00:00:00')
);
foreach ($promos as $p) {
    PromotionManager::createPromotion($p);
}


$prices = array(
    Price::newPrice(NULL, "1", 55),
    Price::newPrice("1", "2", 100),
    Price::newPrice("2", "2", 40)
);

foreach($prices as $p) {
    PriceManager::createPrice($p);
}
$p1 = PriceManager::findPriceById("1");
echo($p1);
assert($p1->getPrice() == 55);
assert(PriceManager::findPriceByPromotionId("2")->getPrice() == 40);

deletePriceTable();
deletePromotionTable();
deleteGameTable();
deleteDeveloperTable();
?>
