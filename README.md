# Deadpool database project 

### โคดเขียนให้ดูเล่นๆ ว่าออกแบบ backend แบบนี้ โอเครกันไหม 

** File structure **
    * setup      -: initialize database, clean database, generate dummy record   
    * models     -: Fetch data from database   
    * controller -: manipulate data, logical  ** ไม่มีแล้วเปลี่ยนเป็น font-end controller ( angularjs )
    * api        -: for apps, ajax    
    * utility    -: utility     
    * sql        -: raw sql query     
   
## อธิบายส่วนของ models,  sql
        ปกติเดี๋ยวนี้คนเขียนเว็บจะเน้นทำให้ sql ใน database ออกมาเป็น object เพราะว่ามันอ่านและเข้าใจได้ง่าย ซึ่งต้นแบบของ object คือ อยู่ในโฟลเดอร์ models ที่ไม่ใช่ *Manager.php
    แล้ว ปกติจะไม่มีการ access ตัว folder sql โดยตรง จะทำงานผ่านตัว Manager ก่อน (เพราะทำให้ดึงวัตถุออกมาเป็น object แล้ว ค่อยเอาข้อมูลมาปั่นอีกทีตรงนี้ ) แต่ไอที่เขียนโครงมานั้นปกติถ้าใช้ framework แม่งทำให้หมดอะ อันนี้ pure php ลำบากหน่อย

## อธิบาย API
    เวลาใช้โปรแกรม หรือ ajax จะสร้าง user หรือลบ จัดการหาข้อมูลต่างๆ จะมาเรียกไฟล์ php ใน folder นี้ แล้วข้อมูลจะส่งหากันแบบ json format

## อธิบาย Controller
    หลังจากใช้ Manager ดึงข้อมูลออกมาแล้ว ก่อนที่จะเอาข้อมูลออกมาหน้าเว็บเนี้ยอาจจำเป็นต้องเอาข้อมูลมา คิดคำนวนอะไรก่อน แล้วค่อยส่งต่อออกไปยังหน้าเว็บก็จะทำที่ส่วนนี้อะ

## utility 
    ใช้เก็บ function ยิบย่อย

## setup 
    ใช้ในการสร้าง database (ตัว install), หรือล้าง database ทิ้ง หรือสร้างข้อมูลใน database ไว้ทดสอบ


** Code Style **    
    Mixes Cases    
        -> Class -: start with capital letter and capital letter between word ( ex. CustomerManager )      
        -> Variables -: use underscore between word (ex. game_name)     
        -> Functions -: start with small letter capital letter between word (ex. createCustomer )    
