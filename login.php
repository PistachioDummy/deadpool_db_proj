<?php session_start(); ?>
<!DOCTYPE html>
<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']); 
    require_once ROOT. '/utility/function.php'; 
    if(isLoggedIn()){
        $url = getBaseURL() . "index.php";
        header("location: {$url}");
        die();
    }
?>
<html lang="en" ng-app='loginApp'>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Deadpool Login</title>

        <!-- CSS -->
        <?php include ROOT . '/templates/header.php'; ?>
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/login/form-elements.css">
        <link rel="stylesheet" href="assets/css/login/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
        <div id="message" >
            <div style="padding: 5px; ">
               <div id="inner-message" class="alert alert-success collapse">
                    <a href="#" class="close" onclick="$('#inner-message').hide()" aria-label="close">&times;</a>
                    <div id="info-message"> </div>
                </div>
             </div>
        </div>
        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Deadpool</strong> Login Form </h1>
                            <div class="description">
                            	<p>
	                            	This is a final project for database class 
                            	</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login to our site</h3>
                            		<p>Enter your username and password to log on:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
                                            <form name="loginForm" role="form" action="#" method="post" class="login-form" 
                                                        ng-controller='loginFormController as loginFormCtrl' 
                                                        ng-submit="loginFormCtrl.submit(loginForm.$valid, $event)">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username" ng-model="loginFormCtrl.form.username" ng-pattern="$scope.userRegex">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password" ng-model="loginFormCtrl.form.password" ng-pattern="$scope.passwordRegex">
			                        </div>
                                                <div class='col-md-12 btn-box'>
                                                    <div class='col-md-6'>
                                                        <button type="submit" ng-click="loginFormCtrl.form.submit = 'Sign In'" class="btn" value="Sign In">Sign in!</button>
                                                    </div>
                                                    <div class='col-md-6'>
                                                    <button type="submit" ng-click="loginFormCtrl.form.submit = 'Register'"  class="btn" value="Register" >Register</button>
                                                    </div>
                                                </div>
			                    </form>
		                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<div class="social-login-buttons">
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- Modal -->
            <div id="registerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" ng-controller="registerFormController as registerCtrl">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="font-style:bold" id="gridSystemModalLabel">Register Form</h4>
                  </div>
                  <div class="modal-body registerform">
                    <div class="row">
                        <div class="form-group">
                            <label for="usr">Name </label>
                        <span style="width:10%"></span>
                            <input type="text" class="form-control regInput" id="usr" ng-model="registerCtrl.form.username" placeholder="Username..">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password </label>
                        <span style="width:10%"></span>
                            <input type="password" class="form-control regInput" id="pwd" ng-model="registerCtrl.form.password" placeholder="Password..">
                        </div>
                        <div class="form-group">
                        <label for="email">Email </label>
                        <span style="width:10%"></span>
                        <input type="text" class="form-control regInput" id="email" ng-model="registerCtrl.form.email" placeholder="Email..">
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="registerCtrl.clearData();">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="registerCtrl.register()">Register</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- Javascript -->
        <?php include ROOT .'/templates/footer.php'; ?>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script src="assets/js/login/app.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
