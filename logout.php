<?php
session_start();
defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
require_once ROOT . "/utility/function.php";
$url = getBaseURL() . "login.php";
$_SESSION["logged_in"] = False;

if(isset($_SESSION["id"])) unset($_SESSION["id"]);
if(isset($_SESSION["profilename"])) unset($_SESSION["profilename"]);
if(isset($_SESSION["username"]))  unset($_SESSION["username"]);
header("location: {$url}");
die();
?>
