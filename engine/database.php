<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/engine/config.php';
    function getConnection(){
        $config = getConfig();
        $HOST = $config['host'];
        $DATABASE_NAME = $config['dbname'];
        $PASSWORD = $config['password'];
        $USERNAME = $config['username'];
        $conn = new PDO("mysql:host=$HOST;dbname=$DATABASE_NAME", $USERNAME, $PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }
    function testConnection(){
        $conn = getConnection();
    }

?>
