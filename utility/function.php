<?php
    function getBaseURL(){
        $pad = "http";
        return $pad . "://" . $_SERVER["SERVER_NAME"] . "/";
    }
    
    function isLoggedIn(){
        if(!is_array($_SESSION)) return False;
        if(!array_key_exists("logged_in", $_SESSION) || $_SESSION["logged_in"] == False){
            return False;
        }
        return True;
    }
?>
