<?php session_start(); ?>
<!doctype html>
<html class="no-js" lang="en" dir="ltr" ng-app='mainApp'>
<?php 
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT. '/utility/function.php'; 
    if(!isLoggedIn()){
        $url = getBaseURL() . "login.php";
        header("location: {$url}");
        die();
    }
?>
    <head>
    <?php include 'templates/header.php'; ?>
    <link rel='stylesheet' href="assets/css/main/navbar.css">
    <link rel='stylesheet' href="assets/css/main/content.css">

    </head>
    <body ng-controller="MenuController as menuCtrl">
        <div id="message" >
            <div style="padding: 5px; ">
               <div id="inner-message" class="alert alert-success">
                    <a href="#" class="close" onclick="$('#inner-message').hide()" aria-label="close">&times;</a>
                    <div id="info-message"> </div>
                </div>
             </div>
        </div>
      <div class="col-sm-2 nav-theme">
        <div class="row">
            <div class="nav-user">
                <div class="nav-user-image">
                    <img src="http://placehold.it/150x150" class="img-circle" alt="Cinque Terre"> 
                </div>
                <div class="nav-user-profile">
                    Welcome, <span class="user-profile" data-toggle="modal" data-target="#editProfile" > {{ menuCtrl.getUser().profilename }} </span> <br /> 
                    <div class="nav-user-money">( {{ menuCtrl.getUser().money | number : 2 }} ฿  ) </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row" >
            <div class="nav-menu-bar" >
                <div class="nav-menu" ng-repeat="menu in menuCtrl.menuList" ng-click="menuCtrl.setSubMenu(menu.id)" ng-class="{ active: menuCtrl.isActive(menu.id) || 0 }">
                    <div class="nav-menu-span">
                        <span class="glyphicon {{ menu.glyphicon }}" aria-hidden="true"></span>
                    </div>  
                        {{ menu.name | uppercase }}
                </div>
                <div class="nav-menu" ng-click="menuCtrl.logOut()">
                    <div class="nav-menu-span">
                        <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                    </div>
                    LOGOUT 
                </div>

            </div>
        </div>
      </div>
      <div class="col-sm-2"></div>
      <div class="col-sm-10">
        <div class="content-wrapper">
            <div class="header">
                <h1> {{ menuCtrl.currentMenu.name }} </h1>
            </div>
            <div class="content-sub-menu">
                <ul class="pagination-sub">
                    <li ng-repeat="subMenu in menuCtrl.currentMenu.subMenu" ng-click="menuCtrl.setSubMenuActive(subMenu.id)" ng-class="{ active: menuCtrl.isSubMenuActive(subMenu.id) }"> {{ subMenu.name }}</li>
                </ul>
            </div>
            <div class="content" >
                <div ng-include src="menuCtrl.viewUrl">
                </div>

            </div>
        </div>
      </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="editProfile">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Profile</h4>
              </div>
    <script>
        window.user = {
                "id": "<?php echo $_SESSION["id"]; ?>",
                "username": "<?php echo $_SESSION["username"]; ?>",
                "profilename": "<?php echo $_SESSION["profilename"]; ?> ",
                "email": "<?php echo $_SESSION["email"]; ?>",
                "money": <?php echo $_SESSION["money"]; ?>
               };
    </script>
    <?php include 'templates/footer.php'; ?>
    <script src="/assets/js/main/app.js"></script>
    <script> jQuery("#inner-message").hide(); </script>
    </body>

</html>

