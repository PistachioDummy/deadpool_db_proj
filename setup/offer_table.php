<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

    function createOfferTable(){
        $sql = '
            CREATE TABLE IF NOT EXISTS Offer
            (
                offer_id        INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
                cust_id         INTEGER NOT NULL,
                price_id        INTEGER NOT NULL,
                offer_time      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                FOREIGN KEY (cust_id) REFERENCES Customer(cust_id)
                    ON UPDATE CASCADE
                    ON DELETE NO ACTION,
                FOREIGN KEY (price_id) REFERENCES Price(price_id)
                    ON UPDATE CASCADE
                    ON DELETE NO ACTION
            );
            ';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }

    function deleteOfferTable(){
        $sql = 'DROP TABLE IF EXISTS Offer;';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }

function clearOfferTable(){
        $sql = 'DELETE FROM Offer;';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
?>
