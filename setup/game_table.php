<?php 
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

function createGameTable(){
    $sql = '
        CREATE TABLE IF NOT EXISTS Game
        (
            game_id         INTEGER PRIMARY KEY AUTO_INCREMENT,
            game_name       varchar(32) NOT NULL,
            parent_game_id  INTEGER,
            dev_id          INTEGER NOT NULL,
            game_image_ref  varchar(255)
            /*
            FOREIGN KEY (parent_game_id) REFERENCES Game(game_id)
                ON UPDATE CASCADE
                ON DELETE CASCADE,
            FOREIGN KEY (dev_id) REFERENCES Developer(dev_id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
             */
        );
    ';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

function deleteGameTable(){
    $sql = 'DROP TABLE IF EXISTS Game;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}
    
function clearGameTable(){
    $sql = 'DELETE FROM Game;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)-execute();
    return $row_count;
}

?>
