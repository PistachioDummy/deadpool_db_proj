<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

function createPromotionTable(){
    $sql = '
        CREATE TABLE IF NOT EXISTS Promotion
        (
            promotion_id    INTEGER NOT NULL AUTO_INCREMENT,
            promotion_name  varchar(32) NOT NULL,
            start_date      TIMESTAMP NOT NULL,
            end_date        TIMESTAMP NOT NULL,
            PRIMARY KEY     (promotion_id)
        );
    ';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

function deletePromotionTable(){
    $sql = '
        DROP TABLE IF EXISTS Promotion;
    ';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

function clearPromotionTable(){
    $sql = 'DELETE FROM IF EXISTS Promotion;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}
?>
