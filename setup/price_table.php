<?php 
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

    function createPriceTable(){
        $sql = '
            CREATE TABLE IF NOT EXISTS Price
            (
                price_id        INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
                promotion_id    INTEGER,
                game_id         INTEGER NOT NULL,
                price           DECIMAL(10, 2) NOT NULL,
                FOREIGN KEY (promotion_id) REFERENCES Promotion(promotion_id)
                    ON UPDATE CASCADE
                    ON DELETE CASCADE,
                FOREIGN KEY (game_id) REFERENCES Game(game_id)
                    ON UPDATE CASCADE
                    ON DELETE CASCADE
            );
        ';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }

    function deletePriceTable(){
        $sql = 'DROP TABLE IF EXISTS Price;';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
        
    function clearPriceTable(){
        $sql = 'DELETE FROM Price;';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }

?>
