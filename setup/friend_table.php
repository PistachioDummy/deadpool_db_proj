<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

function createFriendTable(){
    $sql = '
        CREATE TABLE IF NOT EXISTS Friend
        (
            cust_id         INTEGER NOT NULL,
            friend_id       INTEGER NOT NULL,
            friend_status   INTEGER NOT NULL,
            PRIMARY KEY     (cust_id, friend_id),
            UNIQUE  INDEX   (cust_id, friend_id),
            FOREIGN KEY     (cust_id)   REFERENCES  Customer(cust_id)
                ON DELETE CASCADE,
            FOREIGN KEY     (friend_id) REFERENCES  Customer(cust_id)
                ON DELETE CASCADE
        );
    ';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

function deleteFriendTable(){
    $sql = '
        DROP TABLE IF EXISTS Friend;
    ';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

function clearFriendTable(){
    $sql = 'DELETE FROM Friend;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}
