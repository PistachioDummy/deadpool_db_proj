<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

    function createTopupTable(){
        $sql = '
            CREATE TABLE IF NOT EXISTS Topup
            (
                topup_id        INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
                cust_id         INTEGER NOT NULL,
                topup_money     INTEGER NOT NULL,
                topup_time      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                FOREIGN KEY     (cust_id)   REFERENCES  Customer(cust_id)
                    ON UPDATE CASCADE
                    ON DELETE NO ACTION
            );';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
    
    function deleteTopupTable(){
        $sql = '
            DROP TABLE IF EXISTS Topup;
        ';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
    
    function clearTopupTable(){
        $sql = 'DELETE FROM Topup;';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
?>
