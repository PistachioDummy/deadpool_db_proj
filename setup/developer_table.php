<?php 
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

function createDeveloperTable(){
    $sql = '
        CREATE TABLE IF NOT EXISTS Developer
        (
            dev_id              INTEGER PRIMARY KEY AUTO_INCREMENT,
            dev_name            varchar(32) NOT NULL,
            dev_phonenumber     varchar(32) NOT NULL,
            dev_city            varchar(64) NOT NULL
        );
    ';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

function deleteDeveloperTable(){
    $sql = 'DROP TABLE IF EXISTS Developer;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}
    
function clearDeveloperTable(){
    $sql = 'DELETE FROM Developer;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

?>
