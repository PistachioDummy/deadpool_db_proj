<?php 
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';

    function createRefundTable(){
        $sql = '
            CREATE TABLE IF NOT EXISTS Refund
            (
                refund_id       INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
                offer_id        INTEGER NOT NULL,
                refund_time     TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                FOREIGN KEY (offer_id) REFERENCES Offer (offer_id)
                    ON UPDATE CASCADE
                    ON DELETE RESTRICT
            );
        ';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }

    function deleteRefundTable(){
        $sql = 'DROP TABLE IF EXISTS Refund;';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
        
    function clearRefundTable(){
        $sql = 'DELETE FROM IF EXISTS Refund;';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }

?>
