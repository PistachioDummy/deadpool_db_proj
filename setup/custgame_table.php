<?php
    defined("ROOT") || define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
    require_once ROOT . '/engine/database.php';

function createCustGameTable(){
    $sql = '
        CREATE TABLE IF NOT EXISTS CustGame
        (
            cust_id         INTEGER NOT NULL,
            game_id         INTEGER NOT NULL,
            play_time       INTEGER NOT NULL, 
            PRIMARY KEY     (cust_id, game_id),
            UNIQUE INDEX    (cust_id, game_id),
            FOREIGN KEY     (cust_id) REFERENCES Customer(cust_id)
                ON UPDATE CASCADE
                ON DELETE CASCADE,
            FOREIGN KEY     (game_id) REFERENCES Game(game_id)
                ON UPDATE CASCADE
                ON DELETE CASCADE
        );
    ';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
}

function deleteCustGameTable(){
    $sql = 'DROP TABLE IF EXISTS CustGame;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

function clearCustGameTable(){
    $sql = 'DELETE FROM CustGame;';
    $conn = getConnection();
    $row_count = $conn->prepare($sql)->execute();
    return $row_count;
}

?>
