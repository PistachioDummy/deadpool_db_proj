<?php
    defined("ROOT") || define("ROOT", $_SERVER['DOCUMENT_ROOT']);
    require_once ROOT . '/engine/database.php';
    function createCustomerTable(){
        $sql = '
        CREATE TABLE IF NOT EXISTS Customer 
        (
            cust_id             INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
            cust_username       varchar(32) NOT NULL UNIQUE,
            cust_password       varchar(255) NOT NULL,
            cust_profilename    varchar(32) NOT NULL,
            cust_email          varchar(255) NOT NULL UNIQUE,
            cust_money          DECIMAL(10, 2) UNSIGNED NOT NULL,
            cust_image_ref      varchar(255)
        );';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
    function deleteCustomerTable(){
        $sql = '
            DROP TABLE IF EXISTS Customer;
        ';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
    function clearCustomerTable(){
        $sql = 'DELETE FROM Customer';
        $conn = getConnection();
        $row_count = $conn->prepare($sql)->execute();
        return $row_count;
    }
?>

